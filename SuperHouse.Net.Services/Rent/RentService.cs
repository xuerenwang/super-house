﻿using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Rent;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services
{
    public class RentService : IRentService
    {
        private readonly IRentRepository _repository;
        private readonly IMapper _mapper; //automapper
        public RentService(IRentRepository rent, IMapper mapper)
        {
            _repository = rent;
            _mapper = mapper;
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="rent"></param>
        /// <returns></returns>
        public async Task<int> CreateEmployeeAsync(RentDTO rent)
        {
            Rent rent1 = _mapper.Map<RentDTO, Rent>(rent);
            rent1.CreatedDate = DateTime.Now;
            rent1.ModifiedDate = DateTime.Now;
            //数据持久化
            var result = await _repository.InsertAsync(rent1);
            return result;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(string objId)
        {
            var obj = objId.Split(',');
            Rent j = new Rent();
            bool k = false;
            foreach (var i in obj)
            {
                j = await _repository.GetByIdAsync(i);
                j.Status = 2;
                k = await _repository.UpdateAsync(j);
            }
            return k;
        }
        /// <summary>
        /// 员工信息反填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<RentDTO> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            Rent employee = await _repository.GetByIdAsync(objId);
            return _mapper.Map<Rent, RentDTO>(employee);
        }


        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="rent"></param>
        /// <returns></returns>
        public async Task<PageResultModel<RentDTO>> ShowAsync(RentNewDto rent)
        {
            //拿到结果集
            var pagelist = await _repository.GetAllEmployeePage(rent);
            //转换为DTO返回
            return new PageResultModel<RentDTO>
            {
                Contact = pagelist.Contact,
                Management = pagelist.Management,
                Status=pagelist.Status,
                TotalCount = pagelist.TotalCount,
                PagedData = _mapper.Map<List<Rent>, List<RentDTO>>(pagelist.PagedData)
            };
        }
        /// <summary>
        /// 求租修改
        /// </summary>
        /// <param name="rent"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(RentDTO rent)
        {
            Rent rent1 = _mapper.Map<RentDTO, Rent>(rent);
            rent1.CreatedDate = DateTime.Now;
            rent1.ModifiedDate = DateTime.Now;
            //数据持久化
            var result = await _repository.UpdateAsync(rent1);
            return result;
        }
    }
}
