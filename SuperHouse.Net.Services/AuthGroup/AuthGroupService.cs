﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：角色模块用例服务实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022215 + 新增
 * 
 * *******************************************************************
 */
#endregion
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models.Entities;
using SuperHouse.Net.Models;
using SuperHouse.Net.IServices;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models.Dtos;

namespace SuperHouse.Net.Services
{
    /// <summary>
    /// 角色模块用例服务实现类
    /// </summary>
    public class AuthGroupService : IAuthGroupService
    {
        private readonly IAuthGroupRepository _auth;
        private readonly IMapper _mapper; //automapper
        /// <summary>
        /// 构造函数
        /// </summary>
         public AuthGroupService(IAuthGroupRepository auth, IMapper mapper)
        {
            _auth = auth;
            _mapper = mapper;
        }
        /// <summary>
        /// 角色添加
        /// </summary>
        /// <param name="authGroupDto"></param>
        /// <returns></returns>
        public async Task<int> CreateAuthGroupAsync(AuthGroupDto authGroupDto)
        {
            AuthGroup authgroup = _mapper.Map<AuthGroupDto, AuthGroup>(authGroupDto);
            authgroup.Status = 1;//员工默认状态为正常

            //数据持久化
            var result = await _auth.InsertAsync(authgroup);

            return result;
        }
        /// <summary>
        /// 删除指定角色Id的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(int objId)
        {
            return await _auth.DeleteByIdAsync(objId);
        }
    
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResultModel<AuthGroupDto>> GetAllAuthGroupAsync(AuthGroupPageDto input)
        {

            //拿到结果集
            var pageList = await _auth.GetAllAuthGroupPage(input);
            if (input.Status == 1)
            {
                //转换成用DTO返回
                return new PageResultModel<AuthGroupDto>
                {
                    TotalCount = pageList.TotalCount,
                    PagedData = _mapper.Map<List<AuthGroup>, List<AuthGroupDto>>(pageList.PagedData)
                };
            }
            else
            {
                //转换成用DTO返回
                return new PageResultModel<AuthGroupDto>
                {
                    TotalCount = pageList.TotalCount,
                    PagedData = _mapper.Map<List<AuthGroup>, List<AuthGroupDto>>(pageList.PagedData)
                };
            }

        }
        /// <summary>
        /// 角色信息反填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<AuthGroupDto> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            AuthGroup authGroup = await _auth.GetByIdAsync(objId);
            return _mapper.Map<AuthGroup, AuthGroupDto>(authGroup);
        }
        /// <summary>
        /// 角色修改
        /// </summary>
        /// <param name="authGroupDto"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(AuthGroupDto authGroupDto)
        {
            AuthGroup authGroup = _mapper.Map<AuthGroupDto, AuthGroup>(authGroupDto);
            //数据持久化
            var result = await _auth.UpdateAsync(authGroup);
            return result;
        }
        /// <summary>
        /// 根据主键修改状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> UpdateSoftIdAsync(int Id)
        {
            var inder = await _auth.GetByIdAsync(Id);
            if (inder.Status == 0)
            {
                inder.Status = 1;
            }
            else
            {
                inder.Status = 0;
            }
            return await _auth.UpdateAsync(inder);
        }
    }
}
