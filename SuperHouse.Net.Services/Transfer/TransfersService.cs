﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺转让
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 新增
 * 李博20220125 + 追加(添加)
 * 李博20220126 + 追加(修改、删除、返填)
 * 清20220127 + 代码优化
 * 
 * *******************************************************************
 */
#endregion
using AutoMapper;
using SuperHouse.Net.IRepositories.Transfer;
using SuperHouse.Net.IServices.Transfers;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services.Transfer
{
    /// <summary>
    /// 商铺转让
    /// </summary>
    public class TransfersService : ITransfersService
    {
        private readonly ITransfersRepository _repostransfers;
        private readonly IMapper _mapper; //automapper
       
        public TransfersService(ITransfersRepository repostransfers,
                          IMapper mapper)
        {
            _repostransfers = repostransfers;
            _mapper = mapper;
        }

        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        public async Task<List<TransfersDto>> GetAllUserAsync()
        {
            List<Transfers> transfers = await _repostransfers.GetListAsync();

            return _mapper.Map<List<Transfers>, List<TransfersDto>>(transfers);
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        public async Task<int> CreateEmployeeAsync(CreateTransfersDto createEmployee)
        {
            Transfers employee = _mapper.Map<CreateTransfersDto, Transfers>(createEmployee);
            employee.Status = 1; //默认状态为正常
            employee.Created_date = DateTime.Now; //创建日期
            employee.Modified_date = DateTime.Now;  //更新日期

            //数据持久化
            var result = await _repostransfers.InsertAsync(employee);

            return result;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(int objId)
        {
            return await _repostransfers.DeleteByIdAsync(objId);
        }
       
        /// <summary>
        /// 反填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<TransfersDto> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            Transfers employee = await _repostransfers.GetByIdAsync(objId);

            return _mapper.Map<Transfers, TransfersDto>(employee);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(CreateTransfersDto input)
        {
            Transfers employee = _mapper.Map<CreateTransfersDto, Transfers>(input);
            employee.Modified_date = DateTime.Now;  //更新日期

            //数据持久化
            var result = await _repostransfers.UpdateAsync(employee);

            return result;
        }
        /// <summary>
        /// 显示分页
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageTrabsfersModel<TransfersDto>> GetAllTransfersAsync(TeansfersPageDto input)
        {
            var pageList = await _repostransfers.GetAllEmployeePage(input);
            return new PageTrabsfersModel<TransfersDto>
            {
                TotalCount = pageList.TotalCount,
                PagedData = _mapper.Map<List<Transfers>, List<TransfersDto>>(pageList.PagedData)
            };
        }
    }
}
