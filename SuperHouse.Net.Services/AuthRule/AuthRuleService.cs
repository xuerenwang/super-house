﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：权限模块
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220217 + 新增 增删改查显 （分页 查询）
 * 
 * *******************************************************************
 */
#endregion

using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services
{
    /// <summary>
    /// 权限模块用例服务实现类
    /// </summary>
    public class AuthRuleService : IAuthRuleService
    {
        private readonly IAuthRuleRepository _reposAuthRule;
        private readonly IMapper _mapper; //automapper
        /// <summary>
        /// 构造函数
        /// </summary>
        public AuthRuleService(IAuthRuleRepository reposAuthRule, IMapper mapper)
        {
            _reposAuthRule = reposAuthRule;
            _mapper = mapper;
        }
        /// <summary>
        /// 权限添加
        /// </summary>
        /// <param name="createAuthRule"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> CreateAuthRuleAsync(CreateAuthRuleDto createAuthRule)
        {

            AuthRule authrule = _mapper.Map<CreateAuthRuleDto, AuthRule>(createAuthRule);
            authrule.Status = 1; //权限默认状态为正常

            //数据持久化
            var result = await _reposAuthRule.InsertAsync(authrule);

            return result;
        }
        /// <summary>
        /// 删除指定权限ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> DeleteId(int objId)
        {
            return await _reposAuthRule.DeleteByIdAsync(objId);
        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<PageResultModel<AuthRuleDto>> GetAllAuthRuleAsync(AuthRulePageDto input)
        {
            //拿到结果集
            var pageList = await _reposAuthRule.GetAllAuthRulePage(input);
            //转换成用DTO返回
            return new PageResultModel<AuthRuleDto>
            {
                TotalCount = pageList.TotalCount,
                PagedData = _mapper.Map<List<AuthRule>, List<AuthRuleDto>>(pageList.PagedData)
            };
        }
        /// <summary>
        /// 权限信息返填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<AuthRuleDto> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            AuthRule authrule = await _reposAuthRule.GetByIdAsync(objId);

            return _mapper.Map<AuthRule, AuthRuleDto>(authrule);
        }

        /// <summary>
        /// 权限修改
        /// </summary>
        /// <param name="createAuthRule"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> UpdateAsync(CreateAuthRuleDto input)
        {
            AuthRule authrule = _mapper.Map<CreateAuthRuleDto, AuthRule>(input);

            //数据持久化
            var result = await _reposAuthRule.UpdateAsync(authrule);

            return result;
        }


    }
}
