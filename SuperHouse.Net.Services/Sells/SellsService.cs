﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：史赫森
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：商品出售模块
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220216 + 新增
 * 史赫森20220216 + 追加(添加)
 * 史赫森20220216 + 追加(修改、删除、返填)
 * 史赫森20220216 + 代码优化
 * 王保森20220222 + 添加增加固定字段
 * 王保森20220224 + 修改增加固定字段
 * 
 * *******************************************************************
 */
#endregion
using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Sells;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services
{
   public class SellsService: ISellsService
    {
        private readonly ISellsRepository _reposRepository;
        private readonly IMapper _mapper; //automapper
        //构造函数
        public SellsService(ISellsRepository reposRepository, IMapper mapper)
        {
            _reposRepository = reposRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="createSells"></param>
        /// <returns></returns>
        public async Task<int> CreateSellsAsync(CreateSellsDto createSells)
        {
            Sells sellses = _mapper.Map<CreateSellsDto, Sells>(createSells);
            sellses.Status = 1; //默认状态为正常
            sellses.CreatedDate = DateTime.Now; //创建日期
            sellses.ModifiedDate = DateTime.Now;  //更新日期
            sellses.CustomerId = 1;
            sellses.Type = 1;
            sellses.Area = "1";
            sellses.Price = "1";
            sellses.Contact = "1";
            sellses.IsEmergency = 1;
            sellses.IsSeal = 1;
            sellses.IsDeal = 1;
            sellses.Sort = 1;
            sellses.ClickCount = 1;
            sellses.Introduce = "1";
            sellses.Best = "1";
            sellses.IsShow = 1;
            //数据持久化
            var result = await _reposRepository.InsertAsync(sellses);
            return result;
        }
        /// <summary>
        /// 删除指定商品出售表ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> DeleteId(int objId)
        {
            return await _reposRepository.DeleteByIdAsync(objId);
        }
        /// <summary>
        /// 获取所有的商品出售表数据
        /// </summary>
        /// <returns></returns>
        public async Task<PageResultModel<SellsDto>> GetAllEmployeeAsync(SellsPageDto input)
        {
            var pageList = await _reposRepository.GetAllSellsPage(input);
            return new PageResultModel<SellsDto>
            {
                TotalCount = pageList.TotalCount,
                PagedData = _mapper.Map<List<Sells>, List<SellsDto>>(pageList.PagedData)
            };
        }

        /// <summary>
        /// 商品出售表根据Id查询
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<SellsDto> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            Sells sellses = await _reposRepository.GetByIdAsync(objId);
            return _mapper.Map<Sells, SellsDto>(sellses);
        }
        /// <summary>
        /// 商品出售表表修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(CreateSellsDto input)
        {
            Sells sellses = _mapper.Map<CreateSellsDto, Sells>(input);
            sellses.ModifiedDate = DateTime.Now;  //更新日期
            sellses.CustomerId = 1;
            sellses.Type = 1;
            sellses.Area = "1";
            sellses.Price = "1";
            sellses.Contact = "1";
            sellses.IsEmergency = 1;
            sellses.IsSeal = 1;
            sellses.IsDeal = 1;
            sellses.Sort = 1;
            sellses.ClickCount = 1;
            sellses.Introduce = "1";
            sellses.Best = "1";
            sellses.IsShow = 1;
            //数据持久化
            var result = await _reposRepository.UpdateAsync(sellses);

            return result;
        }

       
    }
}
