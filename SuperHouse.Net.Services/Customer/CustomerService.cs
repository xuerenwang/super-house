﻿using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Customer;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="customerService"></param>
        public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository=customerRepository;
            _mapper = mapper;   
        }
        /// <summary>
        /// 添加客户信息
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>

        public async Task<int> CreateCustomerAsync(CreateCustomerDto createCustomerDto)
        {
            Customer customer=_mapper.Map<CreateCustomerDto,Customer>(createCustomerDto);
            customer.CreatedDate = DateTime.Now;
            var result = await _customerRepository.InsertAsync(customer);   
            return result;
           
        }
        /// <summary>
        /// 删除客户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(string id)
        {
            var obj = id.Split(',');
            Customer j = new Customer();
            bool k = false;
            foreach (var i in obj)
            {
                j = await _customerRepository.GetByIdAsync(i);
                j.Status = 2;
                k = await _customerRepository.UpdateAsync(j);
            }
            return k;
            //return await _customerRepository.DeleteByIdAsync(id);
        }

        /// <summary>
        /// 客户显示
        /// </summary>
        /// <returns></returns>
        public async Task<PageResultModel<CustomerDto>> GetAllCustomerAsync(CustomerPageDto customerPageDto)
        {
           var pageList = await _customerRepository.GetAllCustomerPage(customerPageDto);
            return new PageResultModel<CustomerDto>
            {
                TotalCount = pageList.TotalCount,
                PagedData = _mapper.Map<List<Customer>,List<CustomerDto>>(pageList.PagedData)
            };

         
        }

        //获取客户的id 详情
        public async Task<CustomerDto> GetByIdAsync(int id)
        {
           Customer customer= await _customerRepository.GetByIdAsync(id);
           return _mapper.Map<Customer, CustomerDto>(customer);
          
        }
        /// <summary>
        /// 修改客户信息
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(CreateCustomerDto createCustomerDto)
        {
            Customer customer = _mapper.Map<CreateCustomerDto, Customer>(createCustomerDto);
            customer.ModifiedDate = DateTime.Now;
            var result= await _customerRepository.UpdateAsync(customer);
            return result;
        }
    }
}
