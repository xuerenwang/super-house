﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：员工模块
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 清羽墨安20220125 + 追加(添加)
 * 清羽墨安20220126 + 追加(修改、删除、返填)
 * 清羽墨安20220127 + 代码优化
 * 
 * *******************************************************************
 */
#endregion

using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Services
{
    /// <summary>
    /// 员工模块用例服务实现类
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _reposEmployee;
        private readonly IMapper _mapper; //automapper
        /// <summary>
        /// 构造函数
        /// </summary>
        public EmployeeService(IEmployeeRepository reposEmployee, IMapper mapper)
        {
            _reposEmployee = reposEmployee;
            _mapper = mapper;
        }
        /// <summary>
        /// 员工添加
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> CreateEmployeeAsync(CreateEmployeeDto createEmployee)
        {

            Employee employee = _mapper.Map<CreateEmployeeDto,Employee> (createEmployee);
            employee.Status = 1; //员工默认状态为正常
            employee.CreatedDate = DateTime.Now; //创建日期
            employee.ModifiedDate=DateTime.Now;  //更新日期

            //数据持久化
            var result = await _reposEmployee.InsertAsync(employee);

            return result;
        }
        /// <summary>
        /// 删除指定员工ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> DeleteId(string objId)
        {
            var obj = objId.Split(',');
            Employee j = new Employee();
            bool k = false;
            foreach (var i in obj)
            {
                j = await _reposEmployee.GetByIdAsync(i);
                j.Status = 0;
                k = await _reposEmployee.UpdateAsync(j);
            }
            return k;
        }
        /// <summary>
        /// 根据主键修改状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> UpdateSoftIdAsync(int Id)
        {
            var inder = await _reposEmployee.GetByIdAsync(Id);
            if (inder.Status==0)
            {
                inder.Status = 1;
            }
            else
            {
                inder.Status=0;
            }
            return await _reposEmployee.UpdateAsync(inder);
        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<PageResultModel<EmployeeDto>> GetAllEmployeeAsync(EmployeePageDto input)
        {
            //拿到结果集
            var pageList = await _reposEmployee.GetAllEmployeePage(input);
            if (input.Status==1)
            {
                //转换成用DTO返回
                return new PageResultModel<EmployeeDto>
                {
                    TotalCount = pageList.TotalCount,
                    PagedData = _mapper.Map<List<Employee>, List<EmployeeDto>>(pageList.PagedData)
                };
            }
            else
            {
                //转换成用DTO返回
                return new PageResultModel<EmployeeDto>
                {
                    TotalCount = pageList.TotalCount,
                    PagedData = _mapper.Map<List<Employee>, List<EmployeeDto>>(pageList.PagedData)
                };
            }
            
        }

        /// <summary>
        /// 员工信息返填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<EmployeeDto> GetByIdAsync(int objId)
        {
            //通过主键获取对象
            Employee employee = await _reposEmployee.GetByIdAsync(objId);

            return _mapper.Map<Employee, EmployeeDto>(employee);
        }

        /// <summary>
        /// 员工修改
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> UpdateAsync(CreateEmployeeDto input)
        {
            Employee employee = _mapper.Map<CreateEmployeeDto, Employee>(input);
            employee.ModifiedDate = DateTime.Now;  //更新日期

            //数据持久化
            var result = await _reposEmployee.UpdateAsync(employee);

            return result;
        }


    }
}
