﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：用户模块
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增 王宇行20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using System.Text;
using Newtonsoft.Json;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;

namespace SuperHouse.Net.Services
{
    /// <summary>
    /// 用户模块用例服务实现类
    /// </summary>
    public class UserService : IUserService
    {
        #region 私有字段定义
        private readonly IUserRepository _reposUser;
        private readonly IMapper _mapper; //automapper
        #endregion

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="reposUser"></param>
        /// <param name="mapper"></param>
        public UserService(IUserRepository reposUser,
                           IMapper mapper)
        {
            _reposUser = reposUser;
            _mapper = mapper;
        }

        /// <summary>
        /// 用户添加
        /// </summary>
        /// <param name="createUser"></param>
        /// <returns></returns>
        public async Task<int> CreateUserAsync(CreateUserDto createUser)
        {
            User user = _mapper.Map<CreateUserDto, User>(createUser);
            //user.IsDeleted = false;
            user.CreatedDate = DateTime.Now; //创建日期
            user.ModifiedDate = DateTime.Now;//更新日期

            //数据持久化
            var result = await _reposUser.InsertAsync(user);

            return result;

        }

        /// <summary>
        /// 删除指定用户ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(int objId)
        {
            return await _reposUser.DeleteByIdAsync(objId);
        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        //public async Task<List<UserDto>> GetAllUserAsync()
        //{
        //    List<User> users = await _reposUser.GetListAsync();

        //    return _mapper.Map<List<User>, List<UserDto>>(users);
        //}

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResultModel<UserDto>> GetAlluserrAsync(UserPageDto input)
        {
            //拿到结果集
            var pageList = await _reposUser.GetAllUserPage(input);
            //转换成用DTO返回
            return new PageResultModel<UserDto>
            {
                TotalCount=pageList.TotalCount,
                PagedData=_mapper.Map<List<User>,List<UserDto>>(pageList.PagedData)
            };
        }

        /// <summary>
        /// 用户信息反填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<UserDto> GetByIdAsync(int objId)
        {
            User user = await _reposUser.GetByIdAsync(objId);

            return _mapper.Map<User, UserDto>(user);
        }

        /// <summary>
        /// 用户修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> UpdateAsync(CreateUserDto input)
        {
            User user = _mapper.Map<CreateUserDto, User>(input);
            user.ModifiedDate = DateTime.Now;//更新日期

            //数据持久化
            var result = await _reposUser.UpdateAsync(user);

            return result;
        }

        /// <summary>
        /// 获取用户分页数据
        /// </summary>
        /// <returns></returns>
        public async Task<PageResultDto<UserDto>> GetUserPageListAsync(int pageIndex, int pageSize)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select nick_name as NickName,account as Account, created_date as CreatedDate,modified_date as ModifiedDate, is_deleted as IsDeleted, id ");
            sb.Append("from shop_users");
            sb.Append($" limit {(pageIndex-1)*pageSize},{pageSize}");

            StringBuilder sbCount = new StringBuilder();
            sbCount.Append("select count(1) from shop_users");

            //查询结果集
            var dynList = await _reposUser.QueryFromSqlAsync(sb.ToString());
            //序列化
            string strJson = JsonConvert.SerializeObject(dynList);
            var list = JsonConvert.DeserializeObject<List<UserDto>>(strJson);

            //查询条数
            int n = await _reposUser.GetCountFromSqlAsync(sbCount.ToString());

            return new PageResultDto<UserDto>
            {
                TotalCount = n,
                PagedList = list
            };
        }

        public Task<List<UserDto>> GetAllUserAsync()
        {
            throw new NotImplementedException();
        }

        Task<PageResultDto<UserDto>> IUserService.GetUserPageListAsync(int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
