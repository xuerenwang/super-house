﻿
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：常颖
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：菜单递归
 * 
 * *******************************************************************
 * 修改履历：
 * 常颖20220123 + 新增
 * 
 * *******************************************************************
 */


using AutoMapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.IServices.Menubar;
using SuperHouse.Net.Models.Dtos.Menubar;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SuperHouse.Net.Services.Menubar
{
    public class AdminNavService : IAdminNavService
    {
        //私有字段
        private readonly IAdminNavRepository _reposMenubar;
        private readonly IMapper _mapper; 
        //构造函数
        public AdminNavService(IAdminNavRepository reposMenubar, IMapper mapper)
        {
            _reposMenubar = reposMenubar;
            _mapper = mapper;
        }
        /// <summary>
        /// 菜单添加
        /// </summary>
        /// <param name="AdminNav"></param>
        /// <returns></returns>
        public async Task<int> CreateSellsAsync(ShopAdminNavDTO AdminNav)
        {
            ShopAdminNav customer = _mapper.Map<ShopAdminNavDTO, ShopAdminNav>(AdminNav);
            var result = await _reposMenubar.InsertAsync(customer);
            return result;
        }


        /// <summary>
        /// 菜单的删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteId(int Id)
        {
            return await _reposMenubar.DeleteByIdAsync(Id);
        }

        /// <summary>
        /// 菜单反填
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ShopAdminNavDTO> GetByIdAsync(int Id)
        {
            ShopAdminNav shopAdminNav = await _reposMenubar.GetByIdAsync(Id);

            return _mapper.Map<ShopAdminNav, ShopAdminNavDTO>(shopAdminNav);
        }
        /// <summary>
        /// 菜单的显示
        /// </summary>
        public List<ShopAdminNavDTO> GetMenubars()
        {
            var emp = _reposMenubar.GetList().ToList();
           return _mapper.Map<List<ShopAdminNav>, List<ShopAdminNavDTO>>(emp);
        }

        /// <summary>
        /// 菜单递归
        /// </summary>  
        /// <param name="menubar"></param>
        /// <param name="pid"></param>   
        /// <returns></returns>
        public List<ShopAdminNavDTO> GetTree(List<ShopAdminNavDTO> menubar, int pid = 0)
        {
            var emp = menubar.Where(x => x.PId == pid).ToList();
            if (emp.Count == 0)
            {
                return null;
            }
            List<ShopAdminNavDTO> dtos = new List<ShopAdminNavDTO>(); 
            foreach (var item in emp)
            {
                ShopAdminNavDTO m = new ShopAdminNavDTO();
                m.Id = item.Id;
                m.PId = item.PId;
                m.Name = item.Name;
                m.Mca = item.Mca;
                m.Ico = item.Ico;
                m.OrderNumber = item.OrderNumber;
                m.Ico = item.Ico;
                m.Children = GetTree(menubar, item.Id);
                dtos.Add(m);
            }
            return dtos;
        }


        /// <summary>
        /// 菜单修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(ShopAdminNavDTO input)
        {
            ShopAdminNav shopAdminNav = _mapper.Map<ShopAdminNavDTO, ShopAdminNav>(input);
            var result = await _reposMenubar.UpdateAsync(shopAdminNav);

            return result;
        }
    }
 }