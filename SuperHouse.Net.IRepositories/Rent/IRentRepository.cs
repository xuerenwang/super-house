﻿using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Rent;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 求租表仓储接口
    /// </summary>
    public interface IRentRepository:IBaseRepository<Rent>
    {
        Task<PageResultModel<Rent>> GetAllEmployeePage(RentNewDto rent);
    }
}
