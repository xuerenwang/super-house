﻿using SuperHouse.Net.Models.Dtos.Menubar;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IRepositories
{
    public interface IAdminNavRepository : IBaseRepository<ShopAdminNav>
    {
        Task UpdateAsync(Sells sellses);
    }
}
