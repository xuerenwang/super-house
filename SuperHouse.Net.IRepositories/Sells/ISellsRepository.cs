﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：史赫森
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义商品出售表仓储接口
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220217 + 新增
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Sells;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IRepositories
{/// <summary>
 /// 商品出售表仓储接口
 /// </summary>
    public interface ISellsRepository : IBaseRepository<Sells>
    {
        Task<PageResultModel<Sells>> GetAllSellsPage(SellsPageDto input);
    }
}
