﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义仓储基类接口
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 仓储通用接口类
    /// </summary>
    /// <typeparam name="T">T必须是引用类型,且具有默认构造函数</typeparam>
    public interface IBaseRepository<T>  where T: class ,new()
    {
        /// <summary>
        /// 添加一条实体数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Insert(T entity);

        /// <summary>
        /// 添加一条实体数据（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<int> InsertAsync(T entity);

        /// <summary>
        ///  更新实体数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(T entity);

        /// <summary>
        /// 更新实体数据（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        bool Delete(T entity);

        /// <summary>
        ///  删除数据（异步）
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T entity);

        /// <summary>
        ///  删除指定ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        bool DeleteById(object objId);

        /// <summary>
        ///  删除指定ID的数据（异步）
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<bool> DeleteByIdAsync(object objId);

        /// <summary>
        /// 根据主值查询单条数据
        /// </summary>
        /// <param name="objId">主键值</param>
        /// <returns>泛型实体</returns>
        T GetById(object objId);

        /// <summary>
        /// 根据主值查询单条数据（异步）
        /// </summary>
        /// <param name="objId">主键值</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(object objId);

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        List<T> GetList();

        /// <summary>
        /// 获取全部数据（异步）
        /// </summary>
        /// <returns></returns>
        Task<List<T>> GetListAsync();

        /// <summary>
        /// 执行Sql语句
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="objParams">传参</param>
        /// <returns></returns>
        IEnumerable<dynamic> QueryFromSql(string strSql, object objParams=null);

        /// <summary>
        /// 执行Sql语句（异步）
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        Task<IEnumerable<dynamic>> QueryFromSqlAsync(string strSql, object objParams = null);

        /// <summary>
        /// 执行Sql语句 返回总条数
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="objParams"></param>
        /// <returns></returns>
        int GetCountFromSql(string strSql, object objParams = null);

        /// <summary>
        /// 执行Sql语句 返回总条数（异步）
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="objParams"></param>
        /// <returns></returns>
        Task<int> GetCountFromSqlAsync(string strSql, object objParams = null);
    }
}
