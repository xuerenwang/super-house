﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Customer;
using SuperHouse.Net.Models.Entities;
namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 客户仓储接口
    /// </summary>
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
        Task<PageResultModel<Customer>> GetAllCustomerPage(CustomerPageDto customerPageDto);
    }
}
