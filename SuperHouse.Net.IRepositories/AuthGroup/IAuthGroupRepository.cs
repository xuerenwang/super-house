﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：角色接口
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022215 + 新增
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 角色仓储接口
    /// </summary>
    public interface IAuthGroupRepository : IBaseRepository<AuthGroup>
    {
        Task<PageResultModel<AuthGroup>> GetAllAuthGroupPage(AuthGroupPageDto authGroupPageDto);
    }
}
