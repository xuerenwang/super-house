﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义商铺转让仓储接口
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
namespace SuperHouse.Net.IRepositories.Transfer
{
    /// <summary>
    /// 商铺转让仓储接口
    /// </summary>
    public interface ITransfersRepository : IBaseRepository<Transfers>
    {
        Task<PageTrabsfersModel<Transfers>> GetAllEmployeePage(TeansfersPageDto input);
    }
}
