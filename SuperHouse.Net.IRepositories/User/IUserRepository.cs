﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户仓储接口
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增 王宇行20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models;

namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 用户仓储接口
    /// </summary>
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<PageResultModel<User>> GetAllUserPage(UserPageDto input);
    }
}
