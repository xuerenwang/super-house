﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：定义权限仓储接口
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220216 + 新增
 * 李立豪20220217 + 优化(分页、查询)
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;

namespace SuperHouse.Net.IRepositories
{
    /// <summary>
    /// 权限仓储接口
    /// </summary>
    public interface IAuthRuleRepository : IBaseRepository<AuthRule>
    {
        Task<PageResultModel<AuthRule>> GetAllAuthRulePage(AuthRulePageDto input);
    }
}
