﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户模块用例服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增 王宇行20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;

namespace SuperHouse.Net.IServices
{
    /// <summary>
    /// 用户模块用例服务接口
    /// </summary>
    public interface IUserService
    {
        Task<List<UserDto>> GetAllUserAsync();

        Task<PageResultDto<UserDto>> GetUserPageListAsync(int pageIndex, int pageSize);
        //用户数据显示
        //Task<List<UserDto>> GetAllUserAsync();

       
        // 用户数据显示（分页，查询)
        Task<PageResultModel<UserDto>> GetAlluserrAsync(UserPageDto input);

        //用户添加
        Task<int> CreateUserAsync(CreateUserDto createUser);

        //用户信息修改
        Task<bool> UpdateAsync(CreateUserDto input);

        //删除指定用户id的数据
        Task<bool> DeleteId(int objId);

        //根据主键查询数据
        Task<UserDto> GetByIdAsync(int objId);

    }
}
