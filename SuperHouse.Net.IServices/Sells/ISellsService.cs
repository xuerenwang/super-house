﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：史赫森
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义员工模块用例服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220124 + 新增
 * 史赫森20220125 + 追加(添加)
 * 史赫森20220126 + 追加(修改、删除、返填)
 * 史赫森20220127 + 代码优化
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Sells;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
/// <summary>
/// 商品出售表
/// </summary>
   public interface ISellsService
    {
        /// <summary>
        /// 商品出售表显示
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<SellsDto>> GetAllEmployeeAsync(SellsPageDto input);
        /// <summary>
        /// 商品出售表添加
        /// </summary>
        /// <param name="SellsDto"></param>
        /// <returns></returns>
        Task<int> CreateSellsAsync(CreateSellsDto createSells);
        /// <summary>
        /// 商品出售表修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(CreateSellsDto input);
        /// <summary>
        /// 删除指定商品ID的数据
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        Task<bool> DeleteId(int objId);
        /// <summary>
        /// 根据主键查询商品
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<SellsDto> GetByIdAsync(int objId);
    }
}
