﻿using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Rent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
    /// <summary>
    /// 求租表接口
    /// </summary>
    public interface IRentService
    {
        /// <summary>
        /// 显示 (查询，分页)
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<RentDTO>> ShowAsync(RentNewDto rent);

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<int> CreateEmployeeAsync(RentDTO rent);

        /// <summary>
        /// 员工信息修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(RentDTO rent);
        /// <summary>
        /// 删除指定员工ID的数据
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        Task<bool> DeleteId(string objId);
        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<RentDTO> GetByIdAsync(int objId);
    }
}
