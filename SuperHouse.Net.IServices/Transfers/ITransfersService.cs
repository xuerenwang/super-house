﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义商铺转让模块用例服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 新增
 * 李博20220125 + 追加(添加)
 * 李博20220127 + 追加(修改、删除、返填)
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices.Transfers
{
    /// <summary>
    /// 商铺转让接口
    /// </summary>
    public interface ITransfersService
    {
        Task<PageTrabsfersModel<TransfersDto>> GetAllTransfersAsync(TeansfersPageDto input);
        /// <summary>
        /// 商铺转让显示
        /// </summary>
        /// <returns></returns>
        Task<List<TransfersDto>> GetAllUserAsync();
        /// <summary>
        /// 商铺转让添加
        /// </summary>
        /// <param name="createEmployeeDto"></param>
        /// <returns></returns>
        Task<int> CreateEmployeeAsync(CreateTransfersDto createEmployee);
        /// <summary>
        /// 商铺转让修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(CreateTransfersDto input);
        /// <summary>
        /// 删除指定商铺转让ID的数据
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
          Task<bool> DeleteId(int objId);
        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<TransfersDto> GetByIdAsync(int objId);
    }
}
