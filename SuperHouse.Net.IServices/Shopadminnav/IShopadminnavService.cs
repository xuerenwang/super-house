﻿
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：常颖
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：左侧菜单显示
 * 
 * *******************************************************************
 * 修改履历：
 * 常颖20220123 + 新增
 * 
 * *******************************************************************
 */

using SuperHouse.Net.Models.Dtos.Menubar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices.Menubar
{
   public interface IShopadminnavService
    {
        /// <summary>
        /// 左侧菜单显示
        /// </summary>
        /// <returns></returns>
        List<ShopAdminNavDTO> GetMenubars();
    
        
    }
}
