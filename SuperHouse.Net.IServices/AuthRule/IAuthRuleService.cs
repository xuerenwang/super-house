﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义权限模块用例服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220217 + 新增 增删改查显（分页 查询）
 * 
 * *******************************************************************
 */
#endregion

using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
    /// <summary>
    ///  权限模块用例服务接口
    /// </summary>
    public interface IAuthRuleService
    {
        /// <summary>
        ///权限数据显示(分页、查询)
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<AuthRuleDto>> GetAllAuthRuleAsync(AuthRulePageDto input);
        /// <summary>
        /// 权限添加
        /// </summary>
        /// <param name="createAuthRuleDto"></param>
        /// <returns></returns>
        Task<int> CreateAuthRuleAsync(CreateAuthRuleDto createAuthRule);
        /// <summary>
        /// 权限信息修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(CreateAuthRuleDto input);
        /// <summary>
        /// 删除指定权限ID的数据
        /// </summary>
        /// <param name="createAuthRule"></param>
        /// <returns></returns>
        Task<bool> DeleteId(int objId);
        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<AuthRuleDto> GetByIdAsync(int objId);
    }
}
