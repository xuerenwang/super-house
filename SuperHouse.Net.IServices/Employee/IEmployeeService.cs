﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义员工模块用例服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 清羽墨安20220125 + 追加(添加)
 * 清羽墨安20220126 + 追加(修改、删除、返填)
 * 清羽墨安20220127 + 代码优化
 * 清羽墨安20220216 + 追加(分页、查询)
 * 清羽墨安20220218 + 追加(小修改)
 * 
 * *******************************************************************
 */
#endregion

using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
    /// <summary>
    ///  用户模块用例服务接口
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// 员工数据显示
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<EmployeeDto>> GetAllEmployeeAsync(EmployeePageDto input);
        /// <summary>
        /// 员工添加
        /// </summary>
        /// <param name="createEmployeeDto"></param>
        /// <returns></returns>
        Task<int> CreateEmployeeAsync(CreateEmployeeDto createEmployee);
        /// <summary>
        /// 员工信息修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(CreateEmployeeDto input);
        /// <summary>
        /// 删除指定员工ID的数据
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        Task<bool> DeleteId(string objId);
        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<EmployeeDto> GetByIdAsync(int objId);
        /// <summary>
        /// 根据主键修改状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<bool> UpdateSoftIdAsync(int Id);
    }
}
