﻿using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
    public interface ICustomerService
    {
        /// <summary>
        /// 客户信息显示
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<CustomerDto>> GetAllCustomerAsync(CustomerPageDto customerPageDto);

        /// <summary>
        /// 客户信息添加
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>
        Task<int> CreateCustomerAsync(CreateCustomerDto createCustomerDto);

        /// <summary>
        /// 修改客户信息
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(CreateCustomerDto createCustomerDto);

       /// <summary>
       /// 删除
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        Task<bool> DeleteId(string id);

        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<CustomerDto> GetByIdAsync(int id);
    }
}
