﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：角色接口
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022215 + 新增
 * 
 * *******************************************************************
 */
#endregion
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.IServices
{
    /// <summary>
    ///  角色模块用例服务接口
    /// </summary>
    public interface IAuthGroupService
    {
        /// <summary>
        /// 员工数据显示(分页、查询)
        /// </summary>
        /// <returns></returns>
        Task<PageResultModel<AuthGroupDto>> GetAllAuthGroupAsync(AuthGroupPageDto input);
        /// <summary>
        /// 角色添加
        /// </summary>
        /// <param name="authGroupDto"></param>
        /// <returns></returns>
        Task<int> CreateAuthGroupAsync(AuthGroupDto authGroupDto);
        /// <summary>
        /// 角色修改
        /// </summary>
        /// <param name="authGroupDto"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(AuthGroupDto authGroupDto);
        /// <summary>
        /// 删除指定角色ID的数据
        /// </summary>
        /// <param name="AuthGroupDto"></param>
        /// <returns></returns>
        Task<bool> DeleteId(int objId);
        /// <summary>
        /// 根据主键查询数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<AuthGroupDto> GetByIdAsync(int objId);
        /// <summary>
        /// 根据主键修改状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        Task<bool> UpdateSoftIdAsync(int Id);
    }
}
