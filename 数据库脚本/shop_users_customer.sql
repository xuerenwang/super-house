-- 删除
DROP TABLE IF EXISTS shop_users_customer;
-- 建表语句
CREATE TABLE shop_users_customer(
    id INT(11) NOT NULL auto_increment   COMMENT 'ID' , -- ;auto increment
    user_id INT(11) NOT NULL   COMMENT '用户id' , -- ; 关联用户信息表
    customer_id INT(11) NOT NULL   COMMENT '创建人' , -- ;关联客户信息表
    PRIMARY KEY (id)
) COMMENT = '客户关联表';

INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);
INSERT INTO shop_users_customer VALUES(null,1,1);

-- 显示
SELECT *FROM shop_users_customer

-- 分页查询语句（limit）
select t.* from shop_users_customer t LIMIT 0,2;