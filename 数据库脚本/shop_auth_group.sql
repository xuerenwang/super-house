-- 1. 创建数据库
create database SuperHousedb default charset=utf8;

-- 2. 使用数据库
use SuperHousedb;
DROP TABLE IF EXISTS shop_auth_group;
--建表
create Table shop_auth_group(
id INT(11) NOT NULL AUTO_INCREMENT  COMMENT 'ID',
title char(100) not null COMMENT'标题',
status int(1) not null COMMENT'状态',
rules char(80) not null comment'用户组拥有的权限id',
PRIMARY KEY(id)
)COMMENT="用户组表";
