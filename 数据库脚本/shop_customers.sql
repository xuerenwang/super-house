-- 1、建数据库语句
create database superhousedb;
-- 2 删除
DROP TABLE IF EXISTS shop_customers;
-- 3、建表语句
CREATE TABLE shop_customers(
    id INT(11) NOT  NULL auto_increment  COMMENT 'ID'  ,   -- ;auto increment
    region INT(1)    COMMENT '省市区信息' ,
    classification TINYINT NOT NULL   COMMENT '客户分类',   -- ;1：A类 2：B类: 3：C类、D类、E类' 
    customer_name VARCHAR(20)    COMMENT '客户名称' ,
    customer_address VARCHAR(255)    COMMENT '客户地址' ,
    contact VARCHAR(255)    COMMENT '联系人（销售员）' ,
    telephone VARCHAR(20)    COMMENT '电话号码' ,  -- ;  '管理省市区信息表
    introduce TEXT    COMMENT '详细介绍' ,
    description VARCHAR(255)    COMMENT '跟单描述' ,
    is_deal TINYINT NOT NULL   COMMENT '是否成交' , -- ;'2表示未成交 1表示已成交
    created_date DATETIME    COMMENT '创建日期' ,
    modified_date DATETIME    COMMENT '更新日期' , -- ;2017/02/16更新
    status INT    COMMENT '状态' ,
    sort INT    COMMENT '排序' ,
		PRIMARY KEY (id)
)  COMMENT = '客户信息表';
-- 插入测试语句
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );
INSERT INTO shop_customers VALUES(null, 1,1,'小王','郑州市','阿福','13937403224','详细介绍','详细信息描述',1,'2022/1/22','2022/1/22',1,1 );

-- 显示
SELECT *FROM shop_customers

-- 分页查询语句（limit）
select t.* from shop_customers t LIMIT 0,2;

