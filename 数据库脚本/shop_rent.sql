CREATE TABLE shop_rents(
    id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
    customer_id INT(11) NOT NULL   COMMENT '客户ID' ,
    type tinyint    COMMENT '商铺类型' ,
    requirement tinyint    COMMENT '商铺要求' ,
    title VARCHAR(255)    COMMENT '信息标题' ,
    address VARCHAR(255)    COMMENT '详细地址' ,
    region VARCHAR(255)    COMMENT '求租区域' ,
    min_area VARCHAR(20)    COMMENT '最小求租面积' ,
    max_area VARCHAR(20)    COMMENT '最大求租面积' ,
    price tinyint    COMMENT '转让费' ,
    rentmoney tinyint    COMMENT '租金不超过' ,
    mode tinyint    COMMENT '录入模式' ,
    management tinyint    COMMENT '经营项目' ,
    contact VARCHAR(20)    COMMENT '求租人姓名' ,
    telephone VARCHAR(20)    COMMENT '联系电话' ,
    bind_user VARCHAR(20)    COMMENT '绑定用户' ,
    is_emergency tinyint    COMMENT '是否急转' ,
    is_seal tinyint    COMMENT '是否盖章' ,
    is_deal tinyint    COMMENT '是否成交' ,
    click_count INT(11)    COMMENT '点击率' ,
    introduce text    COMMENT '详细介绍' ,
    sort tinyint    COMMENT '排序' ,
    created_date datetime NOT NULL   COMMENT '发布日期' ,
    modified_date datetime    COMMENT '更新日期' ,
    status tinyint    COMMENT '状态' ,
    best VARCHAR(50)    COMMENT '适合经营' ,
    is_show tinyint    COMMENT '是否显示' ,
    PRIMARY KEY (id)
)  COMMENT = '商铺求租表';

drop table if exists shop_rents
select * from shop_rents

INSERT INTO shop_rents VALUES(1,0,1,1,'出租房屋一','上海市奉贤区上海八维宏烨一','食堂一','10','50',2,1,1,4,'张三','12345678901','李四',1,1,1,1,'食堂的一个窗口一',1,'2022-1-23','2021-1-24',1,'还行',1);
INSERT INTO shop_rents VALUES(2,1,2,3,'出租房屋二','上海市奉贤区上海八维宏烨二','食堂二','20','50',3,2,2,5,'李四','12345678902','王五',2,2,2,2,'食堂的一个窗口二',2,'2022-1-23','2021-1-24',1,'还行',2);
INSERT INTO shop_rents VALUES(3,2,3,2,'出租房屋三','上海市奉贤区上海八维宏烨三','食堂三','30','50',4,3,2,6,'王五','12345678903','宋六',1,2,2,3,'食堂的一个窗口三',3,'2022-1-23','2021-1-24',2,'还行',2);
INSERT INTO shop_rents VALUES(4,3,2,3,'出租房屋四','上海市奉贤区上海八维宏烨四','食堂四','40','50',5,4,1,7,'宋六','12345678904','李七',2,1,2,888,'食堂的一个窗口四',4,'2022-1-23','2021-1-24',2,'还行',1);
INSERT INTO shop_rents VALUES(5,4,1,1,'出租房屋五','上海市奉贤区上海八维宏烨五','食堂五','50','50',6,5,2,8,'李七','12345678905','陈八',2,2,2,4,'食堂的一个窗口五',5,'2022-1-23','2021-1-24',1,'还行',2);

 CREATE  PROCEDURE proc_GetPagedDatatable(
			 shop_rents VARCHAR (20), /*表名*/
       pageIndex INT,  /*当前页*/
       pageSize INT,  /*每页记录数*/
      OUT pageCount INT, /*总记录分页数*/
      OUT totalRecordCount INT  /*总记录数*/
)
BEGIN
    -- 获取表中的记录数
    set @recordCount=0;
    set @sql='';
    set @sql=CONCAT('select count(*) into @recordCount from ',shop_rents);

    prepare stmt from @sql; /*预处理 自定义sql字符串*/
    execute stmt; /*执行自定义sql语句*/
    deallocate prepare stmt; /*释放预处理资源*/
    
    set totalRecordCount = @recordCount; /*总记录数*/
    
    /*计算返回多少页*/
    set @tmp=1;
    set @tmp=@recordCount mod pageSize; /*取余数*/
    if @tmp=0 then
      set pageCount=@recordCount div pageSize;
    else
      set pageCount=@recordCount div pageSize + 1;
    end if;
 
    /*分页显示数据*/
    set @sql=CONCAT('select * from ',shop_rents,' limit ',(pageIndex-1)*pageSize,',',pageSize);

    prepare stmt from @sql; /*预处理 自定义sql字符串*/
    execute stmt;/*执行自定义sql语句*/
    deallocate prepare stmt; /*释放预处理资源*/
 END


CALL proc_GetPagedDatatable(
'`shop_rents`'#表名
,1 #页码
,3 #每页记录数
,@totalcount #输出总记录数
,@pagecount #输出用页数
);
SELECT @totalcount,@pagecount;
