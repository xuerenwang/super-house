

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `shop_transfers`
-- ----------------------------
DROP TABLE IF EXISTS `shop_transfers`;
CREATE TABLE shop_transfers(
    Id INT NOT NULL  auto_increment  COMMENT 'ID' ,
    Customer_id INT NOT NULL   COMMENT '客户ID' ,
    Region VARCHAR(255)    COMMENT '求租区域' ,
    Type TINYINT Not Null   COMMENT '商铺类型;1:沿街商铺 2:商场商铺 3:社区商铺' ,
    Requirement TINYINT Not Null   COMMENT '商铺要求;1:空装 2:带装修 3:不限' ,
    Title VARCHAR(255)     COMMENT '信息标题' ,
    Is_idle  TINYINT Not Null    COMMENT '可否空转;1:是 2:否' ,
    Address VARCHAR(255)    COMMENT '详细地址' ,
    Area VARCHAR(20)    COMMENT '店面面积' ,
    Price VARCHAR(30)    COMMENT '转让价格' ,
    Rent VARCHAR(30)    COMMENT '月租金' ,
    Mode TINYINT Not Null   COMMENT '录入模式;1:前台 2:后台' ,
    Management tinyint Not Null   COMMENT '适合/不适合经营;1:酒楼餐饮 2:服装鞋包 3:美容美发 4:百货超市 5:娱乐休闲 6:汽修美容 7:生活服务 8:空铺转让' ,
    Contact VARCHAR(20)    COMMENT '转让联系人' ,
    Telephone VARCHAR(20)    COMMENT '联系电话' ,
    Bind_user VARCHAR(20)    COMMENT '绑定用户' ,
    Is_emergency TINYINT Not Null    COMMENT '是否急转;1：是 2: 否' ,
    is_seal TINYINT Not Null    COMMENT '是否盖章;1：是 2: 否' ,
    Is_deal TINYINT Not Null    COMMENT '是否成交;1：是 2: 否' ,
    Sort TINYINT Not Null    COMMENT '排序;1：正 2 反' ,
    Click_count INT(11)    COMMENT '点击率' ,
    Introduce VARCHAR(255)    COMMENT '详细介绍' ,
    Status TINYINT Not Null    COMMENT '状态' ,
    Best VARCHAR(50)    COMMENT '适合经营' ,
    Is_show TINYINT Not Null    COMMENT '是否显示;1：是 2 否' ,
    Created_date DATETIME    COMMENT '发布日期' ,
    Modified_date DATETIME    COMMENT '发布日期' ,
    PRIMARY KEY (Id)
)  COMMENT = '商铺转让';

