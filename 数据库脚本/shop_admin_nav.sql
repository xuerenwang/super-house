-- 1、建数据库语句
create database superhousedb;
-- 2 删除
DROP TABLE IF EXISTS shop_admin_nav;
-- 3、建表语句
DROP TABLE IF EXISTS shop_admin_nav;
CREATE TABLE shop_admin_nav(
    id INT(32) NOT NULL AUTO_INCREMENT  COMMENT 'ID' ,
    pid INT    COMMENT '所属父菜单' ,
    name VARCHAR(255)    COMMENT '名称' ,
    mca VARCHAR(255)    COMMENT '模块' ,
    ico VARCHAR(255)    COMMENT '图标' ,
    order_number INT    COMMENT '排序' ,
    PRIMARY KEY (id)
)  COMMENT = '';

-- 插入测试语句
INSERT INTO shop_admin_nav VALUES(null, 0,'总经理','图书',1,1 );
INSERT INTO shop_admin_nav VALUES(null, 1,'副经理','程序',2,2 );
INSERT INTO shop_admin_nav VALUES(null, 2,'秘书','电脑',3,3 );
INSERT INTO shop_admin_nav VALUES(null, 3,'员工','鼠标',4,4 );
-- 显示
SELECT *FROM shop_admin_nav

-- 分页查询语句（limit）
select t.* from shop_admin_nav t LIMIT 0,2;
