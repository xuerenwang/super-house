-- 1. 创建数据库
create database SuperHousedb default charset=utf8;

-- 2. 使用数据库
use SuperHousedb;

-- 创建用户表
DROP TABLE IF EXISTS shop_users;

CREATE TABLE shop_users(
   id INT(11) NOT NULL AUTO_INCREMENT  COMMENT 'ID',
   account VARCHAR(60)  COMMENT '账号',
   `password` VARCHAR(35)  COMMENT '密码',
   nick_name VARCHAR(60)  COMMENT '姓名',
   login_ip VARCHAR(20)  COMMENT '最后登录ip',
   login_count INT(11)  COMMENT '登录次数',
   last_login_time datetime  COMMENT '上次登录时间',
   is_deleted TINYINT(1)  COMMENT '删除状态',
   created_date datetime NOT NULL COMMENT '此条记录被创建时间',
   modified_date datetime NOT NULL COMMENT '此条记录被修改时间',
   PRIMARY KEY (id)
) COMMENT = '用户表';


