-- 创建用户表

DROP TABLE IF EXISTS shop_users;

CREATE TABLE shop_users(
   id INT(11) NOT NULL AUTO_INCREMENT  COMMENT 'ID',
   account VARCHAR(60)  COMMENT '账号',
   `password` VARCHAR(35)  COMMENT '密码',
   nick_name VARCHAR(60)  COMMENT '姓名',
   is_deleted TINYINT(1)  COMMENT '删除状态',
   created_date datetime NOT NULL COMMENT '此条记录被创建时间',
   modified_date datetime NOT NULL COMMENT '此条记录被修改时间',
   PRIMARY KEY (id)
) COMMENT = '用户表';

-- 插入数据
INSERT INTO shop_users VALUES(null, 'zhanghao1','123','王一',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_users VALUES(null, 'zhanghao2','123','王二',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_users VALUES(null, 'zhanghao3','123','王三',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_users VALUES(null, 'zhanghao4','123','王四',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_users VALUES(null, 'zhanghao5','123','王五',1,'2022-01-24','2022-01-24' );


-- 显示
SELECT *FROM shop_users