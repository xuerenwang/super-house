DROP TABLE IF EXISTS shop_employees;
-- 建表
CREATE TABLE shop_employees(
    Id int(11) NOT NULL auto_increment  COMMENT '主键' ,
    name VARCHAR(32) NOT NULL   COMMENT '姓名' ,
    department VARCHAR(20) NOT NULL   COMMENT '部门名称' ,
    telephone VARCHAR(20) NOT NULL   COMMENT '电话号' ,
    email_address VARCHAR(255) NOT NULL   COMMENT '邮箱地址' ,
    position VARCHAR(20) NOT NULL   COMMENT '职位名称' ,
    description VARCHAR(255) NOT NULL   COMMENT '跟单描述' ,
    status int(255) NOT NULL   COMMENT '状态' ,
    created_date DATETIME NOT NULL   COMMENT '创建日期' ,
    modified_date DATETIME NOT NULL   COMMENT '更新日期' ,
    PRIMARY KEY (Id)
)  COMMENT = '员工';

-- 插入数据
INSERT INTO shop_employees VALUES(1, '小修','开发部','11111111111','123@qq.com','开发人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(2, '熊大','销售部','66666666666','123@qq.com','销售人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(3, '熊二','客服部','22222222222','123@qq.com','客服人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(4, '张三','财务部','99999999899','123@qq.com','财务人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(5, '赵四','开发部','66666666666','123@qq.com','开发人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(6, '王五','财务部','88888887599','123@qq.com','财务人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(7, '小六','财务部','55555666687','123@qq.com','财务人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(8, '李琪','客服部','84523658741','123@qq.com','客服人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(9, '团子','客服部','13265478521','123@qq.com','客服人员','xxxxxx',1,'2022-01-24','2022-01-24' );
INSERT INTO shop_employees VALUES(10, '美杜莎','开发部','4587415268','123@qq.com','开发人员','xxxxxx',1,'2022-01-24','2022-01-24' );

-- 显示
SELECT *FROM shop_employees