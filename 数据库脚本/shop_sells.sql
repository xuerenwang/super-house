DROP TABLE IF EXISTS shop_sells;
-- 建表
CREATE TABLE shop_sells(
    id int(11) NOT NULL auto_increment  COMMENT '主键' ,
    customer_id int(11) NOT NULL   COMMENT '客户ID' ,
    region VARCHAR(255) NOT NULL   COMMENT '所在区域' ,
    type INT(1) NOT NULL   COMMENT '商铺类型' ,
    title VARCHAR(255) NOT NULL   COMMENT '信息标题' ,
    address VARCHAR(255) NOT NULL   COMMENT '详细地址' ,
    area VARCHAR(20) NOT NULL   COMMENT '店面面积' ,
    price VARCHAR(20) NOT NULL   COMMENT '出售价格' ,
    mode VARCHAR(20) NOT NULL   COMMENT '录入模式' ,
    contact VARCHAR(20) NOT NULL   COMMENT '出售联系人' ,
    telephone VARCHAR(20) NOT NULL   COMMENT '联系电话' ,
    bind_user VARCHAR(20) NOT NULL   COMMENT '绑定用户' ,
    is_emergency int NOT NULL   COMMENT '是否急转' ,
    is_seal int NOT NULL   COMMENT '是否盖章' ,
    is_deal int NOT NULL   COMMENT '是否成交' ,
    sort int NOT NULL   COMMENT '排序' ,
    click_count int(11) NOT NULL   COMMENT '点击率' ,
    introduce VARCHAR(255) NOT NULL   COMMENT '详细介绍' ,
    `status` int(1) NOT NULL   COMMENT '状态' ,
    best VARCHAR(50) NOT NULL   COMMENT '是和经营' ,
    is_show int(1) NOT NULL   COMMENT '是否显示' ,
    created_date DATETIME NOT NULL   COMMENT '创建日期' ,
    modified_date DATETIME NOT NULL   COMMENT '更新日期' ,
    PRIMARY KEY (id)
)  COMMENT = '商品出售表';

-- 插入数据
INSERT INTO shop_sells VALUES(1,1,'上海市',1,'某某饭店急转','上海奉贤54','120m','5w/m','后台','123434444','1244455555','芝士包',1,1,1,1,200,'老板跟小姨子跑了',1,'饭店',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(2,2,'上海市',1,'某某酒店急转','上海奉贤54','121m','6w/m','前台','123434444','1244455555','杨莫某',1,1,1,1,210,'杨老板跟小姨子跑了',1,'酒店',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(3,3,'上海市',1,'某某旅馆急转','上海奉贤54','122m','7w/m','后台','123434444','1244455555','王莫某',1,1,1,1,220,'王老板跟小姨子跑了',1,'旅馆',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(4,4,'上海市',1,'某某小卖铺急转','上海奉贤54','123m','8w/m','前台','123434444','1244455555','李莫某',1,1,1,1,230,'李老板跟小姨子跑了',1,'小卖铺',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(5,5,'上海市',1,'某某商铺急转','上海奉贤54','124m','9w/m','后台','123434444','1244455555','贺莫某',1,1,1,1,240,'贺老板跟小姨子跑了',1,'商铺',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(6,6,'上海市',1,'某某奶茶店急转','上海奉贤54','125m','10w/m','前台','123434444','1244455555','姜莫某',1,1,1,1,250,'姜老板跟小姨子跑了',1,'奶茶',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(7,7,'上海市',1,'某某网吧急转','上海奉贤54','126m','11w/m','后台','123434444','1244455555','赵莫某',1,1,1,1,260,'赵老板跟小姨子跑了',1,'网吧',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(8,8,'上海市',1,'某某剧本杀急转','上海奉贤54','127m','12w/m','前台','123434444','1244455555','周莫某',1,1,1,1,270,'周老板跟小姨子跑了',1,'剧本杀',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(9,9,'上海市',1,'某某医院急转','上海奉贤54','128m','13w/m','后台','123434444','1244455555','李莫某',1,1,1,1,280,'李老板跟小姨子跑了',1,'医院',1,'2022-02-16','2022-02-18' );
INSERT INTO shop_sells VALUES(10,10,'上海市',1,'某某洗浴中心急转','上海奉贤54','129m','14w/m','前台','123434444','1244455555','张莫某',1,1,1,1,290,'张老板跟小姨子跑了',1,'洗浴中心',1,'2022-02-16','2022-02-18' );
-- 显示
SELECT *FROM shop_sells