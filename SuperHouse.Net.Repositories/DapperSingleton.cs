﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义Dapper单例
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增
 * 雪人王20220123   修改 将配置文件设置移动到构造方法中
 * 
 * *******************************************************************
 */
#endregion

using System.Data;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System.Data.SqlClient;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// Dapper实例（采用单例模式）
    /// </summary>
    public class DapperSingleton
    {
        /// <summary>
        /// 读取配置文件用
        /// </summary>
        private IConfiguration Configuration { get; set; }

        // 返回连接实例        
        private static IDbConnection dbConnection = null;

        // 静态变量保存类的实例    
        private static DapperSingleton uniqueInstance;

        // 定义一个标识确保线程同步     
        private static readonly object locker = new object();

        /// <summary>
        /// 私有构造方法，使外界不能创建该类的实例，以便实现单例模式
        /// </summary>
        private DapperSingleton()
        {
            Configuration = new ConfigurationBuilder()
                .Add(new JsonConfigurationSource
                {
                    Path = "appsettings.json",
                    ReloadOnChange = true
                }).Build();
        }

        /// <summary>
        /// 获取实例，这里为单例模式，保证只存在一个实例
        /// </summary>
        /// <returns></returns>
        public static DapperSingleton GetInstance()
        {
            // 双重锁定实现单例模式，在外层加个判空条件主要是为了减少加锁、释放锁的不必要的损耗
            if (uniqueInstance == null)
            {
                lock (locker)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new DapperSingleton();
                    }
                }
            }
            return uniqueInstance;
        }


        /// <summary>
        /// 创建数据库连接对象（工厂方法）
        /// </summary>
        /// <returns></returns>
        public IDbConnection DbConnectionFactory()
        {
            //如果数据库连接对象为空，则重新创建
            if (dbConnection == null)
            {
                //读取配置文件中的数据库服务器类型 mysql或sqlserver
                string dbType = Configuration["DbServerType"];
                //读取配置文件中的连接字符串
                string _connectionString = Configuration.GetConnectionString("Default");

                switch (dbType)
                {
                    case "mysql":
                        dbConnection = new MySqlConnection(_connectionString);
                        break;
                    case "sqlserver":
                        dbConnection = new SqlConnection(_connectionString);
                        break;
                }
                
            }
            //判断连接状态（Dapper内部会自动打开数据库连接，所以无需以下下代码）
            //if (dbConnection.State == ConnectionState.Closed)
            //{
            //    dbConnection.Open();
            //}

            return dbConnection;
        }
    }
}
