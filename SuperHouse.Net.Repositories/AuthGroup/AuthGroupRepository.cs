﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：角色仓储接口实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022215 + 新增
 * 
 * *******************************************************************
 */
#endregion
using Dapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 角色仓储接口实现类
    /// </summary>
    public class AuthGroupRepository : BaseRepository<AuthGroup>, IAuthGroupRepository
    {
    

        public async Task<PageResultModel<AuthGroup>> GetAllAuthGroupPage(AuthGroupPageDto authGroupPageDto)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_auth_group ");
            //条数
            sbmsql.Append(" select count(1) from  shop_auth_group");

            wheresql.Append(" where 1=1");

            if (!string.IsNullOrEmpty(authGroupPageDto.Title))
            {
                //根据员工姓名查询
                wheresql.Append(" and Name like '%" + authGroupPageDto.Title + "%'");
            }
            if (authGroupPageDto.Status == 1)
            {
                //根据条件查询
                wheresql.Append(" and Status = 1");
            }
            if (authGroupPageDto.Status == 0)
            {
                //根据条件查询
                wheresql.Append(" and Status = 0");
            }
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(authGroupPageDto.PageIndex - 1) * authGroupPageDto.PageSize} ,{authGroupPageDto.PageSize}");
            //拼接总条数条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<AuthGroup>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<AuthGroup> result = new PageResultModel<AuthGroup>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;
            return result; ;
        }
    }
}
