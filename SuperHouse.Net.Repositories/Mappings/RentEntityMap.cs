﻿using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// 求租表到数据库的映射配置
    /// </summary>
    public class RentEntityMap : DommelEntityMap<Rent>
    {
        public RentEntityMap()
        {
            ToTable("shop_rents");//自定义表名
            Map(p => p.Id).IsKey();//主键
            Map(p => p.CustomerId).ToColumn("customer_id", caseSensitive: false);
            Map(p => p.Type).ToColumn("type", caseSensitive: false);
            Map(p => p.Requirement).ToColumn("requirement", caseSensitive: false);
            Map(p => p.Title).ToColumn("title", caseSensitive: false);
            Map(p => p.Address).ToColumn("address", caseSensitive: false);
            Map(p => p.Region).ToColumn("region", caseSensitive: false);
            Map(p => p.MinArea).ToColumn("min_area", caseSensitive: false);
            Map(p => p.MaxArea).ToColumn("max_area", caseSensitive: false);
            Map(p => p.Price).ToColumn("price", caseSensitive: false);
            Map(p => p.RentMoney).ToColumn("rentmoney", caseSensitive: false);
            Map(p => p.Mode).ToColumn("mode", caseSensitive: false);
            Map(p => p.Management).ToColumn("management", caseSensitive: false);
            Map(p => p.Contact).ToColumn("contact", caseSensitive: false);
            Map(p => p.Telephone).ToColumn("telephone", caseSensitive: false);
            Map(p => p.BindUser).ToColumn("bind_user", caseSensitive: false);
            Map(p => p.IsEmergency).ToColumn("is_emergency", caseSensitive: false);
            Map(p => p.IsSeal).ToColumn("is_seal", caseSensitive: false);
            Map(p => p.IsDeal).ToColumn("is_deal", caseSensitive: false);
            Map(p => p.ClickCount).ToColumn("click_count", caseSensitive: false);
            Map(p => p.Introduce).ToColumn("introduce", caseSensitive: false);
            Map(p => p.Sort).ToColumn("sort", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.Best).ToColumn("best", caseSensitive: false);
            Map(p => p.IsShow).ToColumn("is_show", caseSensitive: false);
            Map(p => p.CreatedDate).ToColumn("created_date", caseSensitive: false);
            Map(p => p.ModifiedDate).ToColumn("modified_date", caseSensitive: false);
        }
    }
}
