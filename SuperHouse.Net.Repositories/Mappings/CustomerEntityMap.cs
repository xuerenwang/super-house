﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：赵鑫豪
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：客户映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 赵鑫豪2022215 + 新增
 * *******************************************************************
 */
#endregion
using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    public class CustomerEntityMap : DommelEntityMap<Customer>
    {
       public CustomerEntityMap()
        {
            ToTable("shop_customers");//客户信息表
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Region).ToColumn("region", caseSensitive:false);
            Map(p => p.Classification).ToColumn("classification", caseSensitive: false);
            Map(p => p.CustomerName).ToColumn("customer_name", caseSensitive: false);
            Map(p => p.CustomerAddress).ToColumn("customer_address", caseSensitive: false);
            Map(p => p.Contact).ToColumn("contact", caseSensitive: false);
            Map(p => p.Telephone).ToColumn("telephone", caseSensitive: false);
            Map(p => p.Introduce).ToColumn("introduce", caseSensitive: false);
            Map(p => p.Description).ToColumn("description", caseSensitive: false);
            Map(p => p.IsDeal).ToColumn("is_deal", caseSensitive: false);
            Map(p => p.CreatedDate).ToColumn("created_date", caseSensitive: false);
            Map(p => p.ModifiedDate).ToColumn("modified_date", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.Sort).ToColumn("sort", caseSensitive: false);

        }
    }
}
