﻿using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperHouse.Net.Models.Entities;
namespace SuperHouse.Net.Repositories.Mappings
{
   public class AdminEntityMap : DommelEntityMap<ShopAdminNav>
    {
        public AdminEntityMap()
        {
            ToTable("shop_admin_nav");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.PId).ToColumn("pid", caseSensitive: false);
            Map(p => p.Name).ToColumn("name", caseSensitive: false);
            Map(p => p.Mca).ToColumn("mca", caseSensitive: false);
            Map(p => p.Ico).ToColumn("ico", caseSensitive: false);
            Map(p => p.OrderNumber).ToColumn("order_number", caseSensitive: false);

        }
    }
}
