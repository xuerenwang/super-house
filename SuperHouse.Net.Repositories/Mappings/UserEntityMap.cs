﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：用户模块实体类映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增 王宇行20220216 + 修改
 * 
 * *******************************************************************
 */
#endregion
using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// 实体类到数据库的映射配置
    /// </summary>
    public class UserEntityMap : DommelEntityMap<User>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public UserEntityMap()
        {
            ToTable("shop_users");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Account).ToColumn("account", caseSensitive: false);
            Map(p => p.Password).ToColumn("password",caseSensitive:false);
            Map(p => p.NickName).ToColumn("nick_name", caseSensitive: false);
            Map(p => p.IsDeleted).ToColumn("is_deleted", caseSensitive: false);
            Map(p => p.CreatedDate).ToColumn("created_date", caseSensitive: false);
            Map(p => p.ModifiedDate).ToColumn("modified_date", caseSensitive: false);

        }
    }
}
