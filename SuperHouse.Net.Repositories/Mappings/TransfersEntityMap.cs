﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/1/25
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺转让实体类映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220123 + 新增
 * 
 * *******************************************************************
 */
#endregion
using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    public class TransfersEntityMap : DommelEntityMap<Transfers>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public TransfersEntityMap()
        {
            ToTable("shop_transfers");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Customer_id).ToColumn("customer_id", caseSensitive: false);
            Map(p => p.Region).ToColumn("region", caseSensitive: false);
            Map(p => p.Type).ToColumn("type", caseSensitive: false);
            Map(p => p.Requirement).ToColumn("requirement", caseSensitive: false);
            Map(p => p.Title).ToColumn("title", caseSensitive: false);
            Map(p => p.Is_idle).ToColumn("is_idle", caseSensitive: false);
            Map(p => p.Address).ToColumn("address", caseSensitive: false);
            Map(p => p.Area).ToColumn("area", caseSensitive: false);
            Map(p => p.Price).ToColumn("price", caseSensitive: false);
            Map(p => p.Rent).ToColumn("rent", caseSensitive: false);
            Map(p => p.Mode).ToColumn("mode", caseSensitive: false);
            Map(p => p.Management).ToColumn("management", caseSensitive: false);
            Map(p => p.Contact).ToColumn("contact", caseSensitive: false);
            Map(p => p.Telephone).ToColumn("telephone", caseSensitive: false);
            Map(p => p.Bind_user).ToColumn("bind_user", caseSensitive: false);
            Map(p => p.Is_emergency).ToColumn("is_emergency", caseSensitive: false);
            Map(p => p.Is_seal).ToColumn("is_seal", caseSensitive: false);
            Map(p => p.Is_deal).ToColumn("is_deal", caseSensitive: false);
            Map(p => p.Sort).ToColumn("sort", caseSensitive: false);
            Map(p => p.Click_count).ToColumn("click_count", caseSensitive: false);
            Map(p => p.Introduce).ToColumn("introduce", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.Best).ToColumn("best", caseSensitive: false);
            Map(p => p.Is_show).ToColumn("is_show", caseSensitive: false);
            Map(p => p.Created_date).ToColumn("created_date", caseSensitive: false);
            Map(p => p.Modified_date).ToColumn("modified_date", caseSensitive: false);

        }

    }
}
