﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：角色映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022215 + 新增
 * 邢新悦2022217+修改
 * *******************************************************************
 */
#endregion
using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// 实体类到的映射配置
    /// </summary>
    public class AuthGroupMap : DommelEntityMap<AuthGroup>
    {

        /// <summary>
        /// 构造方法
        /// </summary>
        public AuthGroupMap()
        {
            ToTable("shop_auth_group");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Title).ToColumn("title", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.Rules).ToColumn("rules", caseSensitive: false);

        }
    }
}
