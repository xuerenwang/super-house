﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：用户模块实体类映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增
 * 清羽墨安20220124 + 追加EmployeeEntityMap
 * 李立豪20220217 + 追加AuthRuleEntityMap
 * 
 * *******************************************************************
 */
#endregion
using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// FluentMapper初始化配置
    /// </summary>
    public class DapperFluentMapperConfig
    {
        public static void Initialize()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new UserEntityMap()); //添加用户实体映射配置
                config.AddMap(new EmployeeEntityMap()); //添加员工实体映射配置
                config.AddMap(new CustomerEntityMap()); //添加客户实体映射配置
                config.AddMap(new AuthGroupMap()); //添加角色实体映射配置
                config.AddMap (new RentEntityMap());//求租表实体映射配置
                config.AddMap(new AdminEntityMap()); //添加员工实体映射配置
                config.AddMap(new SellsEntityMap());//商品出售配置
                config.AddMap(new TransfersEntityMap()); //添加转让实体映射配置
                config.AddMap(new AuthRuleEntityMap()); //添加权限实体映射配置
                //...

                config.ForDommel();
            });
        }
    }
}
