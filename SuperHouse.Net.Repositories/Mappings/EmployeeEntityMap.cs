﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：实体类到数据库的映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 
 * *******************************************************************
 */
#endregion

using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// 实体类到数据库的映射配置
    /// </summary>
    public class EmployeeEntityMap : DommelEntityMap<Employee>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public EmployeeEntityMap()
        {
            ToTable("shop_employees");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Name).ToColumn("name", caseSensitive: false);
            Map(p => p.Department).ToColumn("department", caseSensitive: false);
            Map(p => p.Telephone).ToColumn("telephone", caseSensitive: false);
            Map(p => p.EmailAddress).ToColumn("email_address", caseSensitive: false);
            Map(p => p.Position).ToColumn("position", caseSensitive: false);
            Map(p => p.Description).ToColumn("description", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.CreatedDate).ToColumn("created_date", caseSensitive: false);
            Map(p => p.ModifiedDate).ToColumn("modified_date", caseSensitive: false);
        }
    }
}
