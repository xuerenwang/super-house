﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：实体类到数据库的映射配置
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220217 + 新增
 * 
 * *******************************************************************
 */
#endregion

using Dapper.FluentMap.Dommel.Mapping;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Mappings
{
    /// <summary>
    /// 实体类到数据库的映射配置
    /// </summary>
    public class AuthRuleEntityMap : DommelEntityMap<AuthRule>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public AuthRuleEntityMap()
        {
            ToTable("shop_auth_rule");    //自定义表名
            Map(p => p.Id).IsKey();   //主键
            Map(p => p.Title).ToColumn("title", caseSensitive: false);
            Map(p => p.Status).ToColumn("status", caseSensitive: false);
            Map(p => p.Type).ToColumn("type", caseSensitive: false);
            Map(p => p.Name).ToColumn("name", caseSensitive: false);
            Map(p => p.Condition).ToColumn("condition", caseSensitive: false);
        }
    }
}
