﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺转让分页
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 分页
 * 
 * *******************************************************************
 */
#endregion
using Dapper;
using SuperHouse.Net.IRepositories.Transfer;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories.Transfer
{
    /// <summary>
    /// 商铺转让仓库实现类
    /// </summary>
    public class TransfersRepository : BaseRepository<Transfers>, ITransfersRepository
    {
        public async Task<PageTrabsfersModel<Transfers>> GetAllEmployeePage(TeansfersPageDto input)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_transfers");
            //条数
            sbmsql.Append(" select count(1) from shop_transfers");
            wheresql.Append(" where 1=1");
            if (!string.IsNullOrEmpty(input.Contact))
            {
                //根据转让人联系查询
                wheresql.Append(" and Contact like '%" + input.Contact + "%'");
            }
            if (!string.IsNullOrEmpty(input.Bind_User))
            {
                //根据绑定用户查询
                wheresql.Append(" and Bind_user like '%" + input.Bind_User + "%'");
            }
            if(input.Status == 1)
            {
                //根据条件查询
                wheresql.Append(" and status = 1");
            }
            if(input.Status == 0)
            {
                //根据条件查询
                wheresql.Append(" and status = 0");
            }
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(input.PageIndex-1)*input.PageSize},{input.PageSize}");
            //拼接总条数条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<Transfers>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageTrabsfersModel<Transfers> result = new PageTrabsfersModel<Transfers>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;
            return result;


        }
    }
}