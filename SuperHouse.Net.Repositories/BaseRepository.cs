﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义仓储 抽象基类实现
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using SuperHouse.Net.IRepositories;
using Dommel; //该库为Dapper扩展了CRUD方法

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 仓储抽象基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRepository<T>: IBaseRepository<T> where T:class, new()
    {
        //获取数据库连接对象
        protected IDbConnection _db = DapperSingleton.GetInstance().DbConnectionFactory();

        /// <summary>
        /// 添加一条实体数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Insert(T entity) 
        {
            return (int)_db.Insert(entity); 
        }

        /// <summary>
        /// 添加一条实体数据（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> InsertAsync(T entity)
        {
            return Convert.ToInt32(await _db.InsertAsync(entity));
        }

        /// <summary>
        ///  更新实体数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(T entity)
        {
            return _db.Update(entity);
        }

        /// <summary>
        /// 更新实体数据（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(T entity)
        {
            return await _db.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public bool Delete(T entity)
        {
            return _db.Delete(entity);
        }

        /// <summary>
        ///  删除数据（异步）
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(T entity)
        {
            return await _db.DeleteAsync(entity);
        }

        /// <summary>
        ///  删除指定ID的数据
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public bool DeleteById(object objId)
        {
            return Delete(_db.Get<T>(objId));
        }

        /// <summary>
        ///  删除指定ID的数据（异步）
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(object objId)
        {
            return await DeleteAsync(_db.Get<T>(objId));
        }

        /// <summary>
        /// 根据主值查询单条数据
        /// </summary>
        /// <param name="objId">主键值</param>
        /// <returns>泛型实体</returns>
        public T GetById(object objId)
        {
            return _db.Get<T>(objId);
        }

        /// <summary>
        /// 根据主值查询单条数据（异步）
        /// </summary>
        /// <param name="objId">主键值</param>
        /// <returns></returns>
        public async Task<T> GetByIdAsync(object objId)
        {
            return await _db.GetAsync<T>(objId);
        }

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public List<T> GetList()
        {
            return _db.GetAll<T>().ToList();
        }

        /// <summary>
        /// 获取全部数据（异步）
        /// </summary>
        /// <returns></returns>
        public async Task<List<T>> GetListAsync()
        {
            return (await _db.GetAllAsync<T>()).ToList();
        }

        public IEnumerable<dynamic> QueryFromSql(string strSql, object objParams = null)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<dynamic>> QueryFromSqlAsync(string strSql, object objParams = null)
        {
            throw new NotImplementedException();
        }

        public int GetCountFromSql(string strSql, object objParams = null)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetCountFromSqlAsync(string strSql, object objParams = null)
        {
            throw new NotImplementedException();
        }
    }
}
