﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义员工仓库实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 清羽墨安20220216 + 薪增(分页、查询)
 * 清羽墨安20220218 + 追加(根据状态显示)
 * 
 * *******************************************************************
 */
#endregion

using Dapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 员工仓储接口实现类
    /// </summary>
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public async Task<PageResultModel<Employee>> GetAllEmployeePage(EmployeePageDto input)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_employees ");
            //条数
            sbmsql.Append(" select count(1) from  shop_employees");

            wheresql.Append(" where 1=1");

            if (!string.IsNullOrEmpty(input.Name))
            {
                //根据员工姓名查询
                wheresql.Append(" and Name like '%" + input.Name + "%'");

            }
            if (!string.IsNullOrEmpty(input.Department))
            {
                //根据部门查询
                wheresql.Append(" and Department like '%" + input.Department + "%'");
            }
            if (input.Status != 0)
            {
                //根据条件查询
                wheresql.Append(" and Status = 1");
            }else{
                //根据条件查询
                wheresql.Append(" and Status = 0");
            }
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(input.PageIndex - 1) * input.PageSize} ,{input.PageSize}");

            //拼接总条数条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<Employee>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<Employee> result = new PageResultModel<Employee>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;

            return result;
        }
    }
}
