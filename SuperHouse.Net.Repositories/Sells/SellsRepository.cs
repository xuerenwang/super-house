﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：史赫森
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义商品出售表实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220217 + 新增
 * 王保森20220224 + 添加注释 + 修改查询条件
 * 
 * *******************************************************************
 */
#endregion
using Dapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Sells;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 商品出售表分页显示
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public class SellsRepository : BaseRepository<Sells>, ISellsRepository
    {
        /// <summary>
        /// 商品出售表分页显示
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResultModel<Sells>> GetAllSellsPage(SellsPageDto input)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_sells ");
            //条数
            sbmsql.Append(" select count(1) from shop_sells ");
            wheresql.Append(" where 1=1");
            if (!string.IsNullOrEmpty(input.BindUser))
            {
                //根据转让人查询
                wheresql.Append(" and bind_user  like '%" + input.BindUser + "%'");
            }
            if (!string.IsNullOrEmpty(input.Region))
            {
                //根据求租区域查询
                wheresql.Append(" and region  like '%" + input.Region + "%'");
            }
            if (input.Status == 1)
            {
                //根据条件查询
                wheresql.Append(" and status = 1");
            }
            if (input.Status == 0)
            {
                //根据条件查询
                wheresql.Append(" and status = 0");
            }
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(input.PageIndex - 1) * input.PageSize} ,{input.PageSize}");
            //拼接总条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<Sells>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<Sells> result = new PageResultModel<Sells>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;
            return result;
        }
    }
}
