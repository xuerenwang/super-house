﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户仓库实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增
 * 王宇行20220216 + 新增
 * *******************************************************************
 */
#endregion
using Dapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 用户仓库实现类
    /// </summary>
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        /// <summary>
        /// 分页仓储
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResultModel<User>> GetAllUserPage(UserPageDto input)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();

            //结果集
            sbsql.Append(" select * from shop_users ");
            //条数
            sbmsql.Append(" select count(1) from shop_users");

            wheresql.Append(" where 1=1");

            if (!string.IsNullOrEmpty(input.NickName))
            {
                //根据姓名查询
                wheresql.Append(" and nick_name like '%" + input.NickName + "%'");
            }
            //if (!string.IsNullOrEmpty(input.NickName))
            //{
            //    //根据姓名查询
            //    wheresql.Append(" and nick_name = '" + input.NickName + "'");
            //}
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(input.PageIndex - 1) * input.PageSize} ,{input.PageSize}");

            //拼接总条数条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<User>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<User> result = new PageResultModel<User>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;

            return result;
        }
    }
}
