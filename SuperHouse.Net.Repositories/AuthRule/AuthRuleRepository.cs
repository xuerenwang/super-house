﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义权限仓库实现类
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220216 + 新增
 * 李立豪20220217 + 薪增(分页、查询)
 * 
 * *******************************************************************
 */
#endregion

using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SuperHouse.Net.Models;

namespace SuperHouse.Net.Repositories
{
    /// <summary>
    /// 权限仓储接口实现类
    /// </summary>
    public class AuthRuleRepository : BaseRepository<AuthRule>, IAuthRuleRepository
    {
        /// <summary>
        /// 分页仓储
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResultModel<AuthRule>> GetAllAuthRulePage(AuthRulePageDto input)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_auth_rule ");
            //条数
            sbmsql.Append(" select count(1) from  shop_auth_rule");

            wheresql.Append(" where 1=1");

            if (!string.IsNullOrEmpty(input.Title))
            {
                //根据权限标题查询
                wheresql.Append(" and Title like '%" + input.Title + "%'");

            }
            if (!string.IsNullOrEmpty(input.Name))
            {
                //根据权限路径查询
                wheresql.Append(" and Name = '" + input.Name + "'");
            }
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(input.PageIndex - 1) * input.PageSize} ,{input.PageSize}");

            //拼接总条数条件
            sbmsql.Append(wheresql);
            //拿到结果集
            var list = (await _db.QueryAsync<AuthRule>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<AuthRule> result = new PageResultModel<AuthRule>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;

            return result;
        }
    }
}
