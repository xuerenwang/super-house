﻿using Dapper;
using SuperHouse.Net.IRepositories;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Rent;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Repositories
{
    public class RentRepository : BaseRepository<Rent>, IRentRepository
    {
        public async Task<PageResultModel<Rent>> GetAllEmployeePage(RentNewDto rent)
        {
            StringBuilder sbsql = new StringBuilder();
            StringBuilder sbmsql = new StringBuilder();
            StringBuilder wheresql = new StringBuilder();
            //结果集
            sbsql.Append(" select * from shop_rents");
            //条数
            sbmsql.Append(" select count(1) from shop_rents");

            wheresql.Append(" where id!=0");

            if (!string.IsNullOrEmpty(rent.Contact))
            {
                //根据求组人姓名进行查询
                wheresql.Append(" and Contact like '%" + rent.Contact + "%'");
            }

            if (rent.Management != 0)
            {
                //根据求组人姓名进行查询
                wheresql.Append(" and Management = '" + rent.Management + "'");
            }
            wheresql.Append(" and Status = '1'");
            //拼接结果输入条件
            sbsql.Append(wheresql);
            sbsql.Append($" limit {(rent.PageIndex - 1) * rent.PageSize},{rent.PageSize}");
            //拼接总条件
            sbmsql.Append(wheresql);
            //拿到总结果集
            var list = (await _db.QueryAsync<Rent>(sbsql.ToString())).ToList();
            //总条数
            var count = await _db.ExecuteScalarAsync<int>(sbmsql.ToString());
            //返回模型
            PageResultModel<Rent> result = new PageResultModel<Rent>();
            //赋值
            result.TotalCount = count;
            result.PagedData = list;
            return result;
        }
    }
}
