﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/23
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户模块Api控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220123 + 新增 王宇行20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 用户模块Api控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="userService"></param>
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// 获取所有用户数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            //var list = await _userService.GetAllUserAsync();
            var list = await _userService.GetUserPageListAsync(1, 3); // 此处页码和页大小为写死（例子而已）
            return Ok(list);
        }
        [HttpGet]
        public async Task<IActionResult> GetPageList([FromQuery]UserPageDto input) {
            ResponseModel responsemodel = new ResponseModel();
            responsemodel.Code = 1;
            responsemodel.Msg = "用户数据获取成功";
            responsemodel.Data = await _userService.GetAlluserrAsync(input);
            return Ok(responsemodel);
        }
        /// <summary>
        /// 用户添加
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateUserAsync(CreateUserDto createUser) {
            ResponseModel responsemodel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _userService.CreateUserAsync(createUser);
                //验证判断
                if (result>0)
                {
                    responsemodel.Code = result;
                    responsemodel.Msg = "添加用户成功";
                }
                else
                {
                    responsemodel.Code = result;
                    responsemodel.Msg = "添加用户成功";
                }
                return Ok(responsemodel);

            }
            responsemodel.Code = -1;
            responsemodel.Msg = "数据有误";
            return BadRequest(responsemodel);

        }

        /// <summary>
        /// 用户删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteByIdAsync(int objId) {
            try
            {
                ResponseModel responsemodel = new ResponseModel();
                var result = await _userService.DeleteId(objId);
                if (result == true)
                {
                    responsemodel.Code = 1;
                    responsemodel.Msg = "删除用户成功";
                    return Ok(responsemodel);
                }
                else
                {
                    responsemodel.Code = 0;
                    responsemodel.Msg = "删除用户失败";
                    return Ok(responsemodel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        
        }
        /// <summary>
        /// 用户数据反填
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int objId) {
            try
            {
                ResponseModel responsemodel = new ResponseModel();
                responsemodel.Data = await _userService.GetByIdAsync(objId);
                if (responsemodel.Data!=null)
                {
                    responsemodel.Code = 1;
                    responsemodel.Msg = "用户数据查询成功";
                    return Ok(responsemodel);
                }
                else
                {
                    responsemodel.Code = 0;
                    responsemodel.Msg = "用户数据不存在";
                    return Ok(responsemodel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
            
            
            
            

        }
        /// <summary>
        /// 用户数据修改 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(CreateUserDto input) {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _userService.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改用户成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改用户失败";
                    return Ok(responseModel);
                }

            }
            catch (System.Exception)
            {

                throw;
            }
        }


    }
}
