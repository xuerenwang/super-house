﻿/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：常颖
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：控制器
 * 
 * *******************************************************************
 * 新增履历：
 * 常颖20220123 + 新增
 * 
 * *******************************************************************
 */


using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices.Menubar;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos.Menubar;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminNavController : ControllerBase
    {
        private readonly IAdminNavService _IMenubarService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="userService"></param>
        public AdminNavController(IAdminNavService menubarService)
        {
            _IMenubarService = menubarService;
        }
        /// <summary>
        /// 员工修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> UpdateAsync(ShopAdminNavDTO input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _IMenubarService.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "菜单修改成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "菜单修改失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

            [Route("Fan")]
        /// <summary>
        /// 员工数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetById")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                responseModel.Data = await _IMenubarService.GetByIdAsync(id);
                if (responseModel.Data != null)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "菜单查询成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "菜单数据不存在";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        [Route("ADD")]
        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(ShopAdminNavDTO dto)
        {
            ResponseModel res = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _IMenubarService.CreateSellsAsync(dto);
                if (result > 0)
                {
                    res.Code = result;
                    res.Msg = "添加成功";
                }
                else
                {
                    res.Code = result;
                    res.Msg = "添加失败";
                }
                return Ok(res);
            }
            res.Code = -1;
            res.Msg = "数据有误";
            return BadRequest(res);

        }
        [Route("Show")]
        /// <summary>
        /// 左侧菜单显示
        /// </summary>
        [HttpGet]
        public IActionResult MenusShow()
        {
            return Ok(_IMenubarService.GetMenubars());
        }
        /// <summary>
        /// 菜单删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 
        [Route("Delete")]
        [HttpPost]
        public async Task<IActionResult> DeleteByIdAsync(int Id)
        {
            try
            {
                ShopAdminNav shopAdminNav = new ShopAdminNav();
                var result = await _IMenubarService.DeleteId(Id);
                if (result == true)
                {
                    shopAdminNav.Id = 1;
                    shopAdminNav.Mca = "删除菜单成功";
                    return Ok(shopAdminNav);
                }
                else
                {
                    shopAdminNav.Id = 0;
                    shopAdminNav.Mca = "删除菜单失败";
                    return Ok(shopAdminNav);
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
  }

