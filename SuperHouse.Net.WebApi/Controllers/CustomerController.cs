﻿using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Customer;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 客户模块Api控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
                _customerService = customerService;
        }
        /// <summary>
        /// 客户信息数据
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetList([FromQuery] CustomerPageDto customerPageDto )
        {
            try
            {
                ResponseModel model = new ResponseModel();
                model.Code = 200;
                model.Msg = "客户数据获取成功";
                model.Data = await _customerService.GetAllCustomerAsync(customerPageDto);
                return Ok(model);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 添加客户信息
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateCustomerDto createCustomerDto)
        {
            try
            {
                ResponseModel model = new ResponseModel();
                if (ModelState.IsValid)
                {
                    int result = await _customerService.CreateCustomerAsync(createCustomerDto);
                    if (result > 0)
                    {
                        model.Msg = "添加客户信息成功";
                        model.Data = result;
                        model.Code = 200;
                    }
                    else
                    {
                        model = new ResponseModel();
                        model.Code = 400;
                        model.Msg = "添加客户信息失败";
                        model.Data = result;
                    }
                    return Ok(model);
                }
                return BadRequest(model);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 客户的删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteCustomerAsync(string id)
        {
            try
            {
                ResponseModel model = new ResponseModel();
                var result = await _customerService.DeleteId(id);
                if (result == true)
                {
                    model.Code = 1;
                    model.Msg = "删除成功";
                    return Ok(model);
                }
                else
                {
                    model.Msg = "删除失败";
                    model.Code = 0;
                    return Ok(model);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 客户的反填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                
                ResponseModel model = new ResponseModel();
                model.Data = await _customerService.GetByIdAsync(id);
                if (model.Data==null)
                {
                    model.Code= 400;
                    model.Msg = "客户数据反填失败";
                    return Ok(model);
                }
                else
                {
                    model.Code = 200;
                    model.Msg = "客户数据反填成功";
                    return Ok(model);
                }     
              
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 客户修改
        /// </summary>
        /// <param name="createCustomerDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(CreateCustomerDto createCustomerDto)
        {
            ResponseModel model = new ResponseModel();
            var result= await _customerService.UpdateAsync(createCustomerDto);
            if (result==true)
            {
                model.Code = 200;
                model.Msg = "修改客户成功";
                return Ok(model);
            }
            else
            {
                model.Code = 400;
                model.Msg = "修改客户失败";
                return Ok(model);
            }

        }

    }
}
