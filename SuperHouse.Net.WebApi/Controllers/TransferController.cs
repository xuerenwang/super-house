﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：员工模块
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 新增
 * 李博20220125 + 追加(添加)
 * 李博20220126 + 追加(修改、删除、返填)
 * 
 * *******************************************************************
 */
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices.Transfers;
using SuperHouse.Net.Models;//z
using SuperHouse.Net.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 商铺转让控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransferController : ControllerBase
    {
        private readonly ITransfersService _transfersService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="userService"></param>
        public TransferController(ITransfersService transfersService)
        {
            _transfersService = transfersService;
        }
        /// <summary>
        /// 显示分页
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetList([FromQuery] TeansfersPageDto input)
        {

            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "数据获取成功";
            responseModel.Data = await _transfersService.GetAllTransfersAsync(input);
            return Ok(responseModel);
        }


        /// <summary>
        /// 商铺转让添加
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(CreateTransfersDto createEmployee)
        {
            ResponseModel responseModel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _transfersService.CreateEmployeeAsync(createEmployee);
                //验证判断
                if (result > 0)
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加商铺成功";
                }
                else
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加商铺失败";
                }
                return Ok(responseModel);
            }
            responseModel.Code = -1;
            responseModel.Msg = "数据有误";
            return BadRequest(responseModel);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteByIdAsync(int objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _transfersService.DeleteId(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除员工成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除员工失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Data = await _transfersService.GetByIdAsync(id);
            if (responseModel.Data != null)
            {
                responseModel.Code = 1;
                responseModel.Msg = "数据查询成功";
                return Ok(responseModel);
            }
            else
            {
                responseModel.Code = 0;
                responseModel.Msg = "数据不存在";
                return Ok(responseModel);
            }




        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(CreateTransfersDto input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _transfersService.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

    }
}
