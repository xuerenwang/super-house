﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Rent;
using SuperHouse.Net.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RentController : ControllerBase
    {
        private readonly IRentService _service;
        public RentController(IRentService rents)
        {
            _service = rents;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(RentDTO dto)
        {
            ResponseModel res = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _service.CreateEmployeeAsync(dto);
                if (result > 0)
                {
                    res.Code = result;
                    res.Msg = "添加成功";
                }
                else
                {
                    res.Code = result;
                    res.Msg = "添加失败";
                }
                return Ok(res);
            }
            res.Code = -1;
            res.Msg = "数据有误";
            return BadRequest(res);

        }
        /// <summary>
        /// 显示求租表信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Show([FromQuery] RentNewDto rent)
        {
            ResponseModel res = new ResponseModel();
            res.Code = 200;
            res.Msg = "求租表数据获取成功";
            res.Data = await _service.ShowAsync(rent);
            return Ok(res);
        }

        /// <summary>
        /// 员工删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteByIdAsync(string objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _service.DeleteId(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除失败";
                    return Ok(responseModel);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 员工数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "查询成功";
            responseModel.Data = await _service.GetByIdAsync(id);
            return Ok(responseModel);
        }

        /// <summary>
        /// 员工修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(RentDTO input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _service.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
