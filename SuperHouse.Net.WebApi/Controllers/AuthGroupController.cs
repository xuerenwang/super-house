﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：角色控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022216 + 新增
 * 邢新悦2022217+修改
 * *******************************************************************
 */
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 角色模块Api控制器
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthGroupController : ControllerBase
    {
        private readonly IAuthGroupService _auth;
        /// <summary>
        /// 构造函数
        /// </summary>
        public AuthGroupController(IAuthGroupService auth)
        {
            _auth = auth;
        }
        /// <summary>
        /// 角色数据
        /// </summary>
        [HttpGet]
        [Route("Show")]
        public async Task<IActionResult> GetList([FromQuery] AuthGroupPageDto authGroupPageDto)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "角色数据获取成功";
            responseModel.Data = await _auth.GetAllAuthGroupAsync(authGroupPageDto);
            return Ok(responseModel);
        }
        /// <summary>
        /// 角色添加
        /// </summary>
        /// <param name="authGroupDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateAuthGroupAsync(AuthGroupDto authGroupDto)
        {
            ResponseModel responseModel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _auth.CreateAuthGroupAsync(authGroupDto);
                //验证判断
                if (result > 0)
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加角色成功";
                }
                else
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加角色失败";
                }
                return Ok(responseModel);
            }
            responseModel.Code = -1;
            responseModel.Msg = "数据有误";
            return BadRequest(responseModel);
        }
        /// <summary>
        /// 角色删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> DeleteByIdAsync(int Id)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _auth.DeleteId(Id);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除角色成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除角色失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        ///角色数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetById")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try 
            {
                ResponseModel responseModel = new ResponseModel();
                responseModel.Data = await _auth.GetByIdAsync(id);
                if (responseModel.Data != null)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "角色数据查询成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "角色数据不存在";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 角色修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> UpdateAsync(AuthGroupDto authGroupDto)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _auth.UpdateAsync(authGroupDto);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改角色成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改角色失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 根据主键修改员工工作状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateByIdAsync")]
        public async Task<IActionResult> UpdateByIdAsync(int objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _auth.UpdateSoftIdAsync(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "角色状态成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "角色状态失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
