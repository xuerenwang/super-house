﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人:史赫森
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：定义商品出售模块Api控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220216 + 新增
 * 史赫森20220216 + 追加(添加)
 * 史赫森20220216 + 追加(修改、删除、返填)
 * 史赫森20220216 + 代码优化
 * 
 * *******************************************************************
 */
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Sells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SellsController : ControllerBase
    {
        private readonly ISellsService _sellsservice;
        /// <summary>
        /// 构造函数
        /// </summary>
        public SellsController(ISellsService sellsservice)
        {
            _sellsservice = sellsservice;
        }
        /// <summary>
        /// 商品出售表数据
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetList([FromQuery]SellsPageDto input)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "员工数据获取成功";
            responseModel.Data = await _sellsservice.GetAllEmployeeAsync(input);
            return Ok(responseModel);
        }
        /// <summary>
        /// 商品出售添加
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSellsAsync(CreateSellsDto createsells)
        {
            ResponseModel responseModel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _sellsservice.CreateSellsAsync(createsells);
                //验证判断
                if (result > 0)
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加商品出售成功";
                }
                else
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加商品出售失败";
                }
                return Ok(responseModel);
            }
            responseModel.Code = -1;
            responseModel.Msg = "数据有误";
            return BadRequest(responseModel);
        }

        /// <summary>
        /// 商品出售表删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteByIdAsync(int objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _sellsservice.DeleteId(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除商品出售成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除商品出售失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 商品出售表返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "商品出售数据查询成功";
            responseModel.Data = await _sellsservice.GetByIdAsync(id);
            return Ok(responseModel);
        }
        /// <summary>
        /// 商品出售表修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(CreateSellsDto input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _sellsservice.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改商品出售成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改商品出售失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
