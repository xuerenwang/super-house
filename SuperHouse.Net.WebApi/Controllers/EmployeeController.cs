﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户模块Api控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 清羽墨安20220125 + 追加(添加)
 * 清羽墨安20220126 + 追加(修改、删除、返填)
 * 清羽墨安20220127 + 代码优化
 * 清羽墨安20220216 + 追加(分页、查询)
 * 清羽墨安20220217 + 返填完善
 * 清羽墨安20220218 + 追加(小修改)
 * 
 * *******************************************************************
 */
#endregion

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 员工模块Api控制器
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        /// <summary>
        /// 构造函数
        /// </summary>
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        /// <summary>
        /// 员工数据
        /// </summary>
        [HttpGet]
        [Route("Show")]
        public async Task<IActionResult> GetList([FromQuery] EmployeePageDto input)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "员工数据获取成功";
            responseModel.Data = await _employeeService.GetAllEmployeeAsync(input);
            return Ok(responseModel);
        }
        /// <summary>
        /// 员工添加
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateEmployeeAsync(CreateEmployeeDto createEmployee)
        {
            ResponseModel responseModel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _employeeService.CreateEmployeeAsync(createEmployee);
                //验证判断
                if (result > 0)
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加员工成功";
                }
                else
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加员工失败";
                }
                return Ok(responseModel);
            }
            responseModel.Code = -1;
            responseModel.Msg = "数据有误";
            return BadRequest(responseModel);
        }

        /// <summary>
        /// 员工删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> DeleteByIdAsync(string objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _employeeService.DeleteId(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除员工成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除员工失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 根据主键修改员工工作状态
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateByIdAsync")]
        public async Task<IActionResult> UpdateByIdAsync(int objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _employeeService.UpdateSoftIdAsync(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "员工状态成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "员工状态失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 员工数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetById")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                responseModel.Data = await _employeeService.GetByIdAsync(id);
                if (responseModel.Data!=null)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "员工数据查询成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "员工数据不存在";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
            
           
        }
        /// <summary>
        /// 员工修改
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> UpdateAsync(CreateEmployeeDto input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _employeeService.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改员工成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改员工失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
