﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义用户模块Api控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪202202127 + 新增 增删改查显 （分页 查询）
 * 
 * *******************************************************************
 */
#endregion

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHouse.Net.IServices;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Entities;
using System.Threading.Tasks;

namespace SuperHouse.Net.WebApi.Controllers
{
    /// <summary>
    /// 权限模块Api控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthRuleController : ControllerBase
    {
        private readonly IAuthRuleService _authruleService;
        /// <summary>
        /// 构造函数
        /// </summary>
        public AuthRuleController(IAuthRuleService authruleService)
        {
            _authruleService = authruleService;
        }

        /// <summary>
        /// 权限数据
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetList([FromQuery] AuthRulePageDto input)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Code = 1;
            responseModel.Msg = "权限数据获取成功";
            responseModel.Data = await _authruleService.GetAllAuthRuleAsync(input);
            return Ok(responseModel);
        }
        /// <summary>
        /// 权限添加
        /// </summary>
        /// <param name="createAuthRule"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAuthRuleAsync(CreateAuthRuleDto createAuthRule)
        {
            ResponseModel responseModel = new ResponseModel();
            if (ModelState.IsValid)
            {
                int result = await _authruleService.CreateAuthRuleAsync(createAuthRule);
                //验证判断
                if (result > 0)
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加权限成功";
                }
                else
                {
                    responseModel.Code = result;
                    responseModel.Msg = "添加权限失败";
                }
                return Ok(responseModel);
            }
            responseModel.Code = -1;
            responseModel.Msg = "数据有误";
            return BadRequest(responseModel);
        }

        /// <summary>
        /// 权限删除
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteByIdAsync(int objId)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _authruleService.DeleteId(objId);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "删除权限成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "删除权限失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 权限数据返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                responseModel.Data = await _authruleService.GetByIdAsync(id);
                if (responseModel.Data!=null)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "权限数据查询成功!";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "权限数据不存在!";
                    return Ok(responseModel);

                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 权限修改
        /// </summary>
        /// <param name="authrule"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAsync(CreateAuthRuleDto input)
        {
            try
            {
                ResponseModel responseModel = new ResponseModel();
                var result = await _authruleService.UpdateAsync(input);
                if (result == true)
                {
                    responseModel.Code = 1;
                    responseModel.Msg = "修改权限成功";
                    return Ok(responseModel);
                }
                else
                {
                    responseModel.Code = 0;
                    responseModel.Msg = "修改权限失败";
                    return Ok(responseModel);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
