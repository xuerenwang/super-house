﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/19
 * 
 * *******************************************************************
 * 
 * 功能描述：AutoMapper配置文件
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220119 + 新增
 * 清羽墨安20220124 + 追加员工模块Dto映射
 * 赵鑫豪20220126+ 追加客户信息模块Dto映射
 * 常颖 20220124 + 追加员工模块Dto映射
 * 史赫森20220216 + 追加商品出售Dto映射
 * 邢新悦2022216+追加角色模块Dto映射
 * 李博20220124 + 追加商铺转让Dto映射
 * 
 * *******************************************************************
 */
#endregion
using System;
using AutoMapper;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Menubar;
using SuperHouse.Net.Models.Entities;

namespace SuperHouse.Net.AutoMapper
{
    /// <summary>
    /// 领域实体模型 转换为 视图模型（Dto）在这个类中配置
    /// </summary>
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Transfers, TransfersDto>();//商铺转让
            CreateMap<User, UserDto>();
            CreateMap<Employee, EmployeeDto>(); //20220124 追加
            CreateMap<Customer, CustomerDto>(); //赵鑫豪 追加
            CreateMap<AuthGroup, AuthGroupDto>();
            CreateMap<Sells, SellsDto>();//20220216追加
            CreateMap<Rent, RentDTO>(); //贺宇追加
            CreateMap<ShopAdminNav, ShopAdminNavDTO>(); //常颖追加
            CreateMap<AuthGroup, AuthGroupDto>(); //20220216 追加
            CreateMap<AuthRule, AuthRuleDto>(); //20220217 追加
        }
    }
}
