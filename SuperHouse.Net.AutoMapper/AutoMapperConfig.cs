﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/19
 * 
 * *******************************************************************
 * 
 * 功能描述：AutoMapper配置
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220119 + 新增
 * 
 * *******************************************************************
 */
#endregion
using AutoMapper;

namespace SuperHouse.Net.AutoMapper
{
    /// <summary>
    /// AutoMapper配置类
    /// </summary>
    public class AutoMapperConfig
    {
        /// <summary>
        /// 提供静态方法RegisterMappings，一次加载所有层中Profile定义
        /// </summary>
        /// <returns></returns>
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                //这个是领域模型 -> 视图模型的映射，是 读命令
                cfg.AddProfile(new DomainToViewModelMappingProfile());

                //视图模型 → 领域模型的映射 ，是 写命令
                cfg.AddProfile(new ViewModelToDomainMappingProfile());
            });
        }
    }

}
