﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：AutoMapper配置文件
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220119 + 新增
 * 史赫森20220216 + 追加
 * 清羽墨安20220125 + 追加
 * 邢新悦2022216+追加角色表
 * 李立豪20220217 + 追加
 * 
 * *******************************************************************
 */
#endregion
using System;
using AutoMapper;
using SuperHouse.Net.Models;
using SuperHouse.Net.Models.Dtos;
using SuperHouse.Net.Models.Dtos.Sells;
using SuperHouse.Net.Models.Entities;

namespace SuperHouse.Net.AutoMapper
{
    /// <summary>
    /// 视图模型（Dto）转换为领域实体模型 在这个类中配置
    /// </summary>
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            //用户表新增
            CreateMap<CreateUserDto,User>();
            //员工表新增
            CreateMap<CreateEmployeeDto, Employee>();
            //角色表新增
            CreateMap<AuthGroupDto,AuthGroup>();
            CreateMap<RentDTO, Rent>();
            CreateMap<CreateCustomerDto, Customer>();
            //商品出售表新增
            CreateMap<CreateSellsDto, Sells>();
            //商铺转让
            CreateMap<CreateTransfersDto, Transfers>();
            //权限表新增
            CreateMap<CreateAuthRuleDto, AuthRule>();
        }
    }
}
