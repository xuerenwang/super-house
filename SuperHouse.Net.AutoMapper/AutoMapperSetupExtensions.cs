﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：雪人王
 * 创建时间：2022/1/19
 * 
 * *******************************************************************
 * 
 * 功能描述：AutoMapper服务注入扩展方法
 * 
 * *******************************************************************
 * 修改履历：
 * 雪人王20220119 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using SuperHouse.Net.AutoMapper;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 扩展方法，进行AutoMapper的服务依赖注入
    /// </summary>
    public static class AutoMapperSetupExtensions
    {
        //扩展方法
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            //添加服务
            services.AddAutoMapper(typeof(AutoMapperConfig));

            //启动配置
            AutoMapperConfig.RegisterMappings();
        }

    }
}
