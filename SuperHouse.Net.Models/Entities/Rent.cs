﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：贺宇
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺求租表
 * 
 * *******************************************************************
 * 修改履历：
 * 贺宇20220124 + 新增
 * 
 * *******************************************************************
 */
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Entities
{
    /// <summary>
    /// 商铺求租实体
    /// </summary>
    public class Rent
    {
        /// <summary>
        /// ID主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 客户ID
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// 商铺类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 商铺要求
        /// </summary>
        public int Requirement { get; set; }

        /// <summary>
        /// 信息标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 求租区域
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// 最小求租面积
        /// </summary>
        public string MinArea { get; set; }

        /// <summary>
        /// 最大求租面积
        /// </summary>
        public string MaxArea { get; set; }

        /// <summary>
        /// 转让费
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// 租金不超过
        /// </summary>
        public int RentMoney { get; set; }

        /// <summary>
        /// 录入模式
        /// </summary>
        public int Mode { get; set; }

        /// <summary>
        /// 经营项目
        /// </summary>
        public int Management { get; set; }

        /// <summary>
        /// 求租人姓名
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 绑定用户
        /// </summary>
        public string BindUser { get; set; }

        /// <summary>
        /// 是否急转
        /// </summary>
        public int IsEmergency { get; set; }

        /// <summary>
        /// 是否盖章
        /// </summary>
        public int IsSeal { get; set; }

        /// <summary>
        /// 是否成交
        /// </summary>
        public int IsDeal { get; set; }

        /// <summary>
        /// 点击率
        /// </summary>
        public int ClickCount { get; set; }

        /// <summary>
        /// 详细介绍
        /// </summary>
        public string Introduce { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }



        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 适合经营
        /// </summary>
        public string Best { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public int IsShow { get; set; }


        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}
