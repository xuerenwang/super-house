﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：权限实体类
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Entities
{
    /// <summary>
    /// 权限实体类
    /// </summary>
    public class AuthRule
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 权限中文标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 权限路径
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 权限描述
        /// </summary>
        public string Condition { get; set; }
    }
}
