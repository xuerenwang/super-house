﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：员工实体类
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220124 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;


namespace SuperHouse.Net.Models.Entities
{
    /// <summary>
    /// 员工实体类
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Department { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// 职位名称
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// 跟单描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        ///// <summary>
        ///// 构造方法
        ///// </summary>
        //public Employee(int id, string name, string department, string Telephone, string emailAddress, string position, string description, int status, DateTime createdDate, DateTime modifiedDate)
        //{
        //    Id = id;
        //    Name = name;
        //    Department = department;
        //    telephone = Telephone;
        //    EmailAddress = emailAddress;
        //    Position = position;
        //    Description = description;
        //    Status = status;
        //    CreatedDate = createdDate;//创建日期
        //    ModifiedDate = modifiedDate;//更新日期
        //}
    }
}
