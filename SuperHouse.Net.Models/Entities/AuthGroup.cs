﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：用户组表
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦20220215 + 新增
 * 邢新悦2022217+修改
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Entities
{
    /// <summary>
    /// 用户组表
    /// </summary>
    public class AuthGroup
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 用户组拥有的权限：id
        /// </summary>
        public string Rules { get; set; }
    }
}
