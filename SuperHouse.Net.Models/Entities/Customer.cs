﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：赵鑫豪
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：客户信息实体类
 * 
 * *******************************************************************
 * 修改履历：
 * 赵鑫豪20220124 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Entities
{
    /// <summary>
    /// 客户信息表
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 省市区信息
        /// </summary>
        public int Region { get; set; }

        /// <summary>
        /// 客户分类
        /// </summary>
        public int Classification { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 客户地址
        /// </summary>
        public string CustomerAddress { get; set; }

        /// <summary>
        /// 联系人（销售员）
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// 客户电话号码
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 客户介绍
        /// </summary>
        public string Introduce { get; set; }

        /// <summary>
        /// 跟单描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 是否成交
        /// </summary>
        public int IsDeal { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }
        
            
    }
}
