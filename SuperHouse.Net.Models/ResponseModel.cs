﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：返回信息模型类
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220125 + 新增
 * 
 * *******************************************************************
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models
{
    /// <summary>
    /// 返回信息模型类
    /// </summary>
    public class ResponseModel
    {
        /// <summary>    
        ///  返回码   
        /// </summary>   
        public int Code { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public object Data { get; set; }

    }
}
