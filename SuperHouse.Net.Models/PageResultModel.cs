﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：杨利伟
 * 创建时间：2022/2/15
 * 
 * *******************************************************************
 * 
 * 功能描述：分页模型
 * 
 * *******************************************************************
 * 修改履历：
 * 杨利伟2022216 + 新增
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models
{
    /// <summary>
    /// 输入类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResultModel<T>
    {
        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 结果集
        /// </summary>
        public List<T> PagedData { get; set; }
        /// <summary>
        /// 求组人姓名
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 经营项目
        /// </summary>
        public int Management { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
    }
}
