﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺转让分页
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 分页
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models
{
   public class PageTrabsfersModel<T>
    {
        public int TotalCount { get; set; }
        public List<T> PagedData { get; set; }
    }
}
