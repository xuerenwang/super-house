﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：王宇行
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：用户分页集dto
 * 
 * *******************************************************************
 * 修改履历：
 *  王宇行20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models
{
    /// <summary>
    /// 输入参数
    /// </summary>
    public class UserPageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 姓名
        /// </summary>
        public string NickName { get; set; }
    }
}
