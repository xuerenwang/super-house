﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：王宇行
 * 创建时间：2022/1/27
 * 
 * *******************************************************************
 * 
 * 功能描述：用户DTO
 * 
 * *******************************************************************
 * 修改履历：
 * 王宇行20220127 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models
{
    public class CreateUserDto
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public int Id { get; set; }

        [Required(ErrorMessage = "用户账号不能为空")]
        [StringLength(20, ErrorMessage = "用户账号不能超过20")]
        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        [Required(ErrorMessage = "用户姓名不能为空")]
        [StringLength(20, ErrorMessage = "用户姓名不能超过20")]
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        [Required(ErrorMessage = "用户姓名不能为空")]
        [StringLength(20, ErrorMessage = "用户姓名不能超过20")]
        /// <summary>
        /// 姓名
        /// </summary>

        public string NickName { get; set; }

        /// <summary>
        /// 删除状态
        /// </summary>
        public bool IsDeleted { get; set; } = true;

        
    }
}
