﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/1/24
 * 
 * *******************************************************************
 * 
 * 功能描述：员工DTO
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安20220125 + 新增
 * 
 * *******************************************************************
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 员工模块添加更新Dto
    /// </summary>
    public class CreateEmployeeDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; } = 0;
        /// <summary>
        /// 姓名
        /// </summary>
        [Required(ErrorMessage = "姓名不能为空")]
        [StringLength(20, ErrorMessage = "姓名长度不能超过20")]
        public string Name { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [Required(ErrorMessage = "部门名称不能为空")]
        [StringLength(20, ErrorMessage = "部门名称长度不能超过20")]
        public string Department { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Required(ErrorMessage = "电话不能为空")]
        [StringLength(20, ErrorMessage = "电话长度不能超过20")]
        [RegularExpression(@"^1[0-9]{10}$", ErrorMessage = "电话格式不对")]
        public string Telephone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空")]
        [StringLength(255, ErrorMessage = "邮箱长度不能超过255")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "邮箱格式不对")]
        public string EmailAddress { get; set; }
        /// <summary>
        /// 职位名称
        /// </summary>
        [Required(ErrorMessage = "职位名称不能为空")]
        [StringLength(20, ErrorMessage = "职位名称不能超过20")]
        public string Position { get; set; }
        /// <summary>
        /// 员工描述
        /// </summary>
        [Required(ErrorMessage = "员工描述不能为空")]
        [StringLength(255, ErrorMessage = "员工描述不能超过255")]
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// 1:正常   0:禁用
        /// </summary>
        public int Status { get; set; } = 1;
    }
}
