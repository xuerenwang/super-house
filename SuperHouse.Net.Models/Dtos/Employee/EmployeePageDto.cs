﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：清羽墨安
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：定义输入参数类
 * 
 * *******************************************************************
 * 修改履历：
 * 清羽墨安202200216 + 新增(分页、查询输入参数)
 * 清羽墨安202200218 + 追加(员工状态)
 * 
 * *******************************************************************
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 输入参数类
    /// </summary>
    public class EmployeePageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 4;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 员工名称
        /// </summary>
        ///[StringLength(20, ErrorMessage = "姓名长度不能超过20")]
        public string Name { get; set; }
        /// <summary>
        /// 员工部门
        /// </summary>
        ///[StringLength(20, ErrorMessage = "部门名称长度不能超过20")]
        public string Department { get; set; }
        /// <summary>
        /// 员工状态
        /// </summary>
        public int Status { get; set; }
    }
}