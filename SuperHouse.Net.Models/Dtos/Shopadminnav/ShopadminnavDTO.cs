﻿
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：常颖
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：导航菜单DTO
 * 
 * *******************************************************************
 * 修改履历：
 * 常颖20220123 + 新增
 * 
 * *******************************************************************
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos.Menubar
{
   public class ShopAdminNavDTO
    {
        /// <summary>
        /// 导航菜单ID
        /// </summary>
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 导航菜单所属父菜单
        /// </summary>
        public int PId { get; set; }
        /// <summary>
        /// 导航菜单名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 导航菜单模块
        /// </summary>
        public string Mca { get; set; }
        /// <summary>
        /// 导航菜单图标
        /// </summary>
        public string Ico { get; set; }
        /// <summary>
        /// 导航菜单排序
        /// </summary>
        public int OrderNumber { get; set; }

        public List<ShopAdminNavDTO> Children { get; set; }
    }
}
