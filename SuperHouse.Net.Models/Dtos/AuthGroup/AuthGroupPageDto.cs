﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：邢新悦
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：角色DTO
 * 
 * *******************************************************************
 * 修改履历：
 * 邢新悦2022216 + 新增
 * 邢新悦2022217+修改
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 输入参数类
    /// </summary>
    public class AuthGroupPageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 角色状态
        /// </summary>
        public int Status { get; set; }
    }
}

