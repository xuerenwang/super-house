﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：史赫森
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：商品出售Dto类
 * 
 * *******************************************************************
 * 修改履历：
 * 史赫森20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
  public class SellsDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; } = 0;
        /// <summary>
        /// 客户Id
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// 所在区域
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// 商铺类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 信息标题
        /// </summary>
        public string Tittle { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 店面面积
        /// </summary>
        public string Area { get; set; }
        /// <summary>
        /// 出售价格
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 录入模式
        /// </summary>
        public string Mode { get; set; }
        /// <summary>
        /// 出售联系人
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// 绑定用户
        /// </summary>
        public string BindUser { get; set; }
        /// <summary>
        /// 是否急转
        /// </summary>
        public int IsEmergency { get; set; }
        /// <summary>
        /// 是否盖章
        /// </summary>
        public int IsSeal { get; set; }
        /// <summary>
        /// 是否成交
        /// </summary>
        public int IsDeal { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 点击率
        /// </summary>
        public int ClickCount { get; set; }
        /// <summary>
        /// 详细介绍
        /// </summary>
        public string Introduce { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 适合经营
        /// </summary>
        public string Best { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int IsShow { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }

    }
}
