﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos.Sells
{
   public class SellsPageDto
    {
    /// <summary>
    /// 页大小
    /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 转让人 
        /// </summary>
        public string BindUser { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// 转让地址
        /// </summary>
        public int Status { get; set; }
    }
}
