﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    public class CreateCustomerDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 省市区信息
        /// </summary>
        public int Region { get; set; }

        /// <summary>
        /// 客户分类
        /// </summary>
        [Required(ErrorMessage = "不能为空")]
        public int Classification { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        /// 
        [StringLength(20, ErrorMessage = "姓名长度不能超过20")]
        public string CustomerName { get; set; }

        /// <summary>
        /// 客户地址
        /// </summary>
        /// 
        [StringLength(20, ErrorMessage = "地址长度不能超过255")]
        public string CustomerAddress { get; set; }

        /// <summary>
        /// 联系人（销售员）
        /// </summary>
        /// 
        [StringLength(20, ErrorMessage = "联系人长度不能超过20")]
        public string Contact { get; set; }

        /// <summary>
        /// 客户电话号码
        /// </summary>
        /// 
        [StringLength(20, ErrorMessage = "客户电话长度不能超过20")]
        public string Telephone { get; set; }

        /// <summary>
        /// 客户介绍
        /// </summary>
        public string Introduce { get; set; }

        /// <summary>
        /// 跟单描述
        /// </summary>
        /// 
        [StringLength(20, ErrorMessage = "描述长度不能超过255")]
        public string Description { get; set; }

        /// <summary>
        /// 是否成交
        /// </summary>
        [Required(ErrorMessage = "不能为空")]
        public int IsDeal { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}
