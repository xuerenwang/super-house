﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos.Customer
{
    public class CustomerPageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 客户分类
        /// </summary>
        public int Classification { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        [StringLength(20, ErrorMessage = "姓名长度不能超过20")]
        public string CustomerName { get; set; }
        /// <summary>
        /// 客户电话号码
        /// </summary>
        [StringLength(20, ErrorMessage = "客户电话长度不能超过20")]
        public string Telephone { get; set; }

    }
}
