﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：定义输入参数类
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪202200217 + 新增(分页、查询输入参数)
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    public class AuthRulePageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 权限中文标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 权限路径
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 权限描述
        /// </summary>
        public string Condition { get; set; }

    }
}
