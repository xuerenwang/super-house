﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李立豪
 * 创建时间：2022/2/16
 * 
 * *******************************************************************
 * 
 * 功能描述：权限DTO
 * 
 * *******************************************************************
 * 修改履历：
 * 李立豪20220216 + 新增
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 权限模块添加更新Dto
    /// </summary>
    public class CreateAuthRuleDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 权限中文标题
        /// </summary>
        [Required(ErrorMessage = "标题不能为空")]
        [StringLength(20, ErrorMessage = "标题长度不能超过20")]
        public string Title { get; set; }
        /// <summary>
        /// 状态
        /// 1:正常   0:禁用
        /// </summary>
        public int Status { get; set; } = 1;
        /// <summary>
        /// 类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 权限路径
        /// </summary>
        [Required(ErrorMessage = "权限路径不能为空")]
        [StringLength(80, ErrorMessage = "权限路径长度不能超过80")]
        public string Name { get; set; }
        /// <summary>
        /// 权限描述
        /// </summary>
        [Required(ErrorMessage = "权限描述不能为空")]
        [StringLength(100, ErrorMessage = "权限描述长度不能超过100")]
        public string Condition { get; set; }
    }
}
