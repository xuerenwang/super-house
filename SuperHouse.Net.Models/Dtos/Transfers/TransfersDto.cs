﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 商铺转让表
    /// </summary>
    public class TransfersDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 客户ID
        /// </summary>
        public int Customer_id { get; set; }
        /// <summary>
        /// 求租区域
        /// </summary>
        [Required(ErrorMessage = "求租区域不能为空")]
        [StringLength(20, ErrorMessage = "求租区域长度不能超过20")]
        public string Region { get; set; }
        /// <summary>
        /// 商铺类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 商铺要求
        /// </summary>
        public int Requirement { get; set; }
        /// <summary>
        /// 信息标题
        /// </summary>
        [Required(ErrorMessage = "信息标题不能为空")]
        [StringLength(20, ErrorMessage = "信息标题长度不能超过20")]
        public string Title { get; set; }
        /// <summary>
        /// 可否空转
        /// </summary>
        public int Is_idle { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        [Required(ErrorMessage = "详细地址不能为空")]
        [StringLength(20, ErrorMessage = "详细地址长度不能超过20")]
        public string Address { get; set; }
        /// <summary>
        /// 店面面积
        /// </summary>
        [Required(ErrorMessage = "店面面积不能为空")]
        [StringLength(20, ErrorMessage = "店面面积长度不能超过20")]
        public string Area { get; set; }
        /// <summary>
        /// 转让价格
        /// </summary>
        [Required(ErrorMessage = "转让价格不能为空")]
        [StringLength(20, ErrorMessage = "转让价格长度不能超过20")]
        public string Price { get; set; }
        /// <summary>
        /// 月租金
        /// </summary>
        [Required(ErrorMessage = "月租金不能为空")]
        [StringLength(20, ErrorMessage = "月租金长度不能超过20")]
        public string Rent { get; set; }
        /// <summary>
        /// 录入模式
        /// </summary>
        public int Mode { get; set; }
        /// <summary>
        /// 适合/不适合经营
        /// </summary>
        public int Management { get; set; }
        /// <summary>
        /// 转让联系人
        /// </summary>
        [Required(ErrorMessage = "转让联系人不能为空")]
        [StringLength(20, ErrorMessage = "转让联系人长度不能超过20")]
        public string Contact { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [StringLength(20, ErrorMessage = "电话长度不能超过20")]
        [RegularExpression(@"^1[0-9]{10}$", ErrorMessage = "电话格式不对")]
        public string Telephone { get; set; }
        /// <summary>
        /// 绑定用户
        /// </summary>
        [Required(ErrorMessage = "绑定用户不能为空")]
        [StringLength(20, ErrorMessage = "绑定用户长度不能超过20")]
        public string Bind_user { get; set; }
        /// <summary>
        /// 是否急转
        /// </summary>
        public int Is_emergency { get; set; }
        /// <summary>
        /// 是否盖章
        /// </summary>
        public int Is_seal { get; set; }
        /// <summary>
        /// 是否成交
        /// </summary>
        public int Is_deal { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 点击率
        /// </summary>
        public int Click_count { get; set; }
        /// <summary>
        /// 详细介绍
        /// </summary>
        [Required(ErrorMessage = "详细介绍不能为空")]
        [StringLength(20, ErrorMessage = "详细介绍长度不能超过20")]
        public string Introduce { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 适合经营
        /// </summary>
        [Required(ErrorMessage = "适合经营不能为空")]
        [StringLength(20, ErrorMessage = "适合经营长度不能超过20")]
        public string Best { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int Is_show { get; set; }
        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime Created_date { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
       public DateTime Modified_date { get; set; }
    }
}
