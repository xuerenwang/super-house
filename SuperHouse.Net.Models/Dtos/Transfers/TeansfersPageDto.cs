﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：上海雪峰科技有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：李博
 * 创建时间：2022/2/17
 * 
 * *******************************************************************
 * 
 * 功能描述：商铺转让分页
 * 
 * *******************************************************************
 * 修改履历：
 * 李博20220124 + 分页
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 商铺转让分页
    /// </summary>
   public class TeansfersPageDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 联系转让人
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 绑定用户
        /// </summary>
        public string Bind_User { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
    }
}
