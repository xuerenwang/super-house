﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 商铺转让
    /// </summary>
   public class CreateTransfersDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; } = 0;
        /// <summary>
        /// 客户ID
        /// </summary>
        [Required(ErrorMessage = "客户ID不能为空")]
        public int Customer_id { get; set; }
        /// <summary>
        /// 求租区域
        /// </summary>
        [Required(ErrorMessage = "求租区域不能为空")]

        public string Region { get; set; }
        /// <summary>
        /// 商铺类型
        /// </summary>
        [Required(ErrorMessage = "商铺类型不能为空")]

        public int Type { get; set; }
        /// <summary>
        /// 商铺要求
        /// </summary>
        [Required(ErrorMessage = "商铺要求不能为空")]

        public int Requirement { get; set; }
        /// <summary>
        /// 信息标题
        /// </summary>
        [Required(ErrorMessage = "信息标题不能为空")]
        public string Title { get; set; }
        /// <summary>
        /// 可否空转
        /// </summary>
        public int Is_idle { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 店面面积
        /// </summary>
        public string Area { get; set; }
        /// <summary>
        /// 转让价格
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 月租金
        /// </summary>
        public string Rent { get; set; }
        /// <summary>
        /// 录入模式
        /// </summary>
        public int Mode { get; set; }
        /// <summary>
        /// 适合/不适合经营
        /// </summary>
        public int Management { get; set; }
        /// <summary>
        /// 转让联系人
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// 绑定用户
        /// </summary>
        public string Bind_user { get; set; }
        /// <summary>
        /// 是否急转
        /// </summary>
        public int Is_emergency { get; set; }
        /// <summary>
        /// 是否盖章
        /// </summary>
        public int Is_seal { get; set; }
        /// <summary>
        /// 是否成交
        /// </summary>
        public int Is_deal { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 点击率
        /// </summary>
        public int Click_count { get; set; }
        /// <summary>
        /// 详细介绍
        /// </summary>
        public string Introduce { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 适合经营
        /// </summary>
        public string Best { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public int Is_show { get; set; }
        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime Created_date { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime Modified_date { get; set; }
    }
}
