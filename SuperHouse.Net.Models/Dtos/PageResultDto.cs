﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 通用分页数据模型类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResultDto<T> where T : class, new()
    {
        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 分页结果集
        /// </summary>
        public List<T> PagedList { get; set; }
    }
}
