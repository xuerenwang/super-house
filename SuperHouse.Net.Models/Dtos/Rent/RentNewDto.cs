﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos.Rent
{
    /// <summary>
    /// 没有验证的求租表Dto
    /// </summary>
    public class RentNewDto
    {
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 页容量
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 求组人姓名
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 经营项目
        /// </summary>
        public int Management { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

    }
}
