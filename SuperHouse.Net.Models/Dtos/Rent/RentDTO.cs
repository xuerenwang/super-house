﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHouse.Net.Models.Dtos
{
    /// <summary>
    /// 商铺求租表的DTO
    /// </summary>
    public class RentDTO
    {
        /// <summary>
        /// ID主键
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// 客户ID
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// 商铺类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 商铺要求
        /// </summary>
        public int Requirement { get; set; }

        /// <summary>
        /// 信息标题
        /// </summary>
        [Required(ErrorMessage = "标题不能为空")]
        public string Title { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        [Required(ErrorMessage = "详细地址不能为空")]
        public string Address { get; set; }

        /// <summary>
        /// 求租区域
        /// </summary>
        [Required(ErrorMessage = "求租区域不能为空")]
        public string Region { get; set; }

        /// <summary>
        /// 最小求租面积
        /// </summary>
        [Required(ErrorMessage = "最小面积不能为空")]
        public string MinArea { get; set; }

        /// <summary>
        /// 最大求租面积
        /// </summary>
        [Required(ErrorMessage = "最大面积不能为空")]
        public string MaxArea { get; set; }

        /// <summary>
        /// 转让费
        /// </summary>
        [Required(ErrorMessage = "转让费不能为空")]
        public int Price { get; set; }

        /// <summary>
        /// 租金不超过
        /// </summary>
        public int RentMoney { get; set; }

        /// <summary>
        /// 录入模式
        /// </summary>
        public int Mode { get; set; }

        /// <summary>
        /// 经营项目
        /// </summary>
        public int Management { get; set; }

        /// <summary>
        /// 求租人姓名
        /// </summary>
        [Required(ErrorMessage = "求组人姓名不能为空")]
        public string Contact { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        [Required(ErrorMessage = "联系电话不能为空")]
        public string Telephone { get; set; }

        /// <summary>
        /// 绑定用户
        /// </summary>
        [Required(ErrorMessage = "绑定用户不能为空")]
        public string BindUser { get; set; }

        /// <summary>
        /// 是否急转
        /// </summary>
        public int IsEmergency { get; set; }

        /// <summary>
        /// 是否盖章
        /// </summary>
        public int IsSeal { get; set; }

        /// <summary>
        /// 是否成交
        /// </summary>
        public int IsDeal { get; set; }

        /// <summary>
        /// 点击率
        /// </summary>
        public int ClickCount { get; set; }

        /// <summary>
        /// 详细介绍
        /// </summary>
        [Required(ErrorMessage = "详细不能为空")]
        public string Introduce { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 适合经营
        /// </summary>
        [Required(ErrorMessage = "适合经营不能为空")]
        public string Best { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public int IsShow { get; set; }


        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}
