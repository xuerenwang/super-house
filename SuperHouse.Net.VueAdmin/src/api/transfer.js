import request from '../utils/request';

//添加
export const AddTransfer = data => {
    return request({
        url: 'https://localhost:44376/api/Transfer/CreateEmployee',
        method: 'post',
        data: data
    });
};

//删除
export const delTransfer = query => {
    return request({
        url: 'https://localhost:44376/api/Transfer/DeleteById',
        method: 'get',
        params: query
    });
};

//修改
export const updateTransfer = query => {
    return request({
        url: 'https://localhost:44376/api/Transfer/Update',
        method: 'post',
        data: query
    });
}

//分页显示查询
export const showTransfer = query => {
    return request({
        url: 'https://localhost:44376/api/Transfer/GetList',
        method: 'get',
        params: query
    });
};

//反填
export const getTransfer = query =>{
    return request({
        url: 'https://localhost:44376/api/Transfer/GetById',
        method: 'post',
        params: query
    });
}