import request from '../utils/request';

//求租表的显示
export const ShowRent = query => {
    return request({
        url: 'https://localhost:44376/api/Rent/Show',
        method: 'get',
        params: query
    });
};

//求租表的添加
export const addRent = data => {
    return request({
        url: 'https://localhost:44376/api/Rent/Add',
        method: 'post',
        data: data
    });
};

//主键返填
export const getRent = query => {
    return request({
        url: 'https://localhost:44376/api/Rent/GetById',
        method: 'post',
        params: query
    });
};
//小修改/删除主键
export const deleRent = query => {
    return request({
        url: 'https://localhost:44376/api/Rent/DeleteById',
        method: 'post',
        params: query
    });
};

//修改
export const updateRent = query => {
    return request({
        url: 'https://localhost:44376/api/Rent/Update',
        method: 'post',
        data: query
    });
};