import request from '../utils/request';
//角色分页显示查询
export const AuthShow = query => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/Show',
        method: 'get',
        params: query
    });
};
//角色添加
export const AddAuthGroup = data => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/Create',
        method: 'post',
        data: data
    });
};
//角色修改
export const updateAuth = query => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/Update',
        method: 'post',
        data: query
    });
};
//角色返填
export const getAuth = query => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/GetById',
        method: 'post',
        params: query
    });
};
//角色逻辑删除
export const deleAuth = query => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/UpdateByIdAsync',
        method: 'post',
        params: query
    });
};
//角色物理删除
export const delAuth = query => {
    return request({
        url: 'https://localhost:44376/api/AuthGroup/Delete',
        method: 'get',
        params: query
    });
};