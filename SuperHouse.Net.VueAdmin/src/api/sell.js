import request from '../utils/request';

//添加
export const addSell = data => {
    return request({
        url: 'https://localhost:44376/api/Sells/CreateSells',
        method: 'post',
        data: data
    });
};

//分页显示查询
export const showSell = query => {
    return request({
        url: 'https://localhost:44376/api/Sells/GetList',
        method: 'get',
        params: query
    });
};

//删除
export const delSell = query => {
    return request({
        url: 'https://localhost:44376/api/Sells/DeleteByIdAsync',
        method: 'get',
        params: query
    });
};

//修改
export const updateSell = query => {
    return request({
        url: 'https://localhost:44376/api/Sells/Update',
        method: 'post',
        data: query
    });
}

//反填
export const getSell = query =>{
    return request({
        url: 'https://localhost:44376/api/Sells/GetById',
        method: 'post',
        params: query
    });
}