import request from '../utils/request';

//求租表的显示
export const ShowRent = query => {
    return request({
        url: 'https://localhost:44376/api/Rent/Show',
        method: 'get',
        params: query
    });
};