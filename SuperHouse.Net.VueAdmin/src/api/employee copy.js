import request from '../utils/request';

//添加
export const AddEmp = data => {
    return request({
        url: 'https://localhost:5001/api/Employee/CreateEmployee',
        method: 'post',
        data: data
    });
};

//删除
export const delEmp = query => {
    return request({
        url: '',
        method: 'get',
        params: query
    });
};

//修改