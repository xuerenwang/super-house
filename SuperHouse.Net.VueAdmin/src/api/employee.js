import request from '../utils/request';

//添加
export const AddEmp = data => {
    return request({
        url: 'https://localhost:44376/api/Employee/Create',
        method: 'post',
        data: data
    });
};
//分页显示查询
export const showEmp = query => {
    return request({
        url: 'https://localhost:44376/api/Employee/Show',
        method: 'get',
        params: query
    });
};
//修改
export const updateEmp = query => {
    return request({
        url: 'https://localhost:44376/api/Employee/Update',
        method: 'post',
        data: query
    });
};
//小修改
export const deleEmp = query => {
    return request({
        url: 'https://localhost:44376/api/Employee/UpdateByIdAsync',
        method: 'post',
        params: query
    });
};
//批量删除/修改
export const deleEmps = query => {
    return request({
        url: 'https://localhost:44376/api/Employee/Delete',
        method: 'post',
        params: query
    });
};
//返填
export const getEmp = query => {
    return request({
        url: 'https://localhost:44376/api/Employee/GetById',
        method: 'post',
        params: query
    });
};