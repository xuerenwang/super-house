import request from '../utils/request';

//客户表的显示
export const ShowCustomer = query => {
    return request({
        url: 'https://localhost:44376/api/Customer/GetList',
        method: 'get',
        params: query
    });
};
//添加
export const AddCustomer = data => {
    return request({
        url: 'https://localhost:44376/api/Customer/CreateCustomer',
        method: 'post',
        data: data
    });
};
//删除
export const DeleCustomer = query => {
    return request({
        url: 'https://localhost:44376/api/Customer/DeleteCustomer',
        method: 'post',
        params: query
    });
};
//批量删除/修改
export const DeleCustomers = query => {
    return request({
        url: 'https://localhost:44376/api/Customer/DeleteCustomer',
        method: 'post',
        params: query
    });
};
//返填
export const GetCustomer = query => {
    return request({
        url: 'https://localhost:44376/api/Customer/GetById',
        method: 'post',
        params: query
    });
};
//修改
export const UpDateCustomer = query => {
    return request({
        url: 'https://localhost:44376/api/Customer/Update',
        method: 'post',
        data: query
    });
};