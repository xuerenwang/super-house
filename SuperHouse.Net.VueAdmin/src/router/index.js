import {createRouter, createWebHistory} from "vue-router";
import Home from "../views/Home.vue";

const routes = [
    {
        path: '/',
        redirect: '/dashboard'
    }, {
        path: "/",
        name: "Home",
        component: Home,
        children: [
            {
                path: "/dashboard",
                name: "dashboard",
                meta: {
                    title: '导航菜单'
                },
                component: () => import (
                /* webpackChunkName: "dashboard" */
                "../views/Dashboard.vue")
            },
            {
                path: "/table",
                name: "basetable",
                meta: {
                    title: '员工'
                },
                component: () => import (
                /* webpackChunkName: "table" */
                "../views/Employee/EmployeeTable.vue")
            }, 
            {
                path: "/SellTable",
                name: "SellTable",
                meta: {
                    title: '商铺出租'
                },
                component: () => import (
                /* webpackChunkName: "table" */
                "../views/Sell/SellTable.vue")
            }, 
            {
                path: "/CustomerShow",
                name: "CustomerShow",
                meta: {
                    title: '客户管理'
                },
                component: () => import (/* webpackChunName: "charts" */
                "../views/Customer/CustomerShow.vue")
            },
            {
                path: "/TransfersShow",
                name: "TransfersShow",
                meta: {
                    title: '转让'
                },
                component: () => import (
                /* webpackChunkName: "charts" */
                "../views/Transfer/TransfersShow.vue")
            }, 
            {
                path: "/form",
                name: "baseform",
                meta: {
                    title: '入职'
                },
                component: () => import (
                /* webpackChunkName: "form" */
                "../views/Employee/AddEmp.vue")
            }, 
            {
                path: "/tabs",
                name: "tabs",
                meta: {
                    title: '客户'
                },
                component: () => import (
                /* webpackChunkName: "tabs" */
                "../views/Tabs.vue")
            },
            {
                path: '/CustomerTable',
                name: 'CustomerTable',
                meta: {
                    title: '客户表的显示'
                },
                component: () => import (/* webpackChunkName: "403" */
                '../views/Customer/CustomerTable.vue')
            }, 
            {
                path: "/donate",
                name: "donate",
                meta: {
                    title: '鼓励作者'
                },
                component: () => import (
                /* webpackChunkName: "donate" */
                "../views/Donate.vue")
            }, 
            {
                path: "/permission",
                name: "permission",
                meta: {
                    title: '权限管理',
                    permission: true
                },
                component: () => import (
                /* webpackChunkName: "permission" */
                "../views/Permission.vue")
            }, 
            {
                path: "/roles",
                name: "roles",
                meta: {
                    title: '角色',
                    permission: true
                },
                component: () => import (
                /* webpackChunkName: "permission" */
                "../views/System/AuthGroup.vue")
            }, 
            {
                path: "/addroles",
                name: "addroles",
                meta: {
                    title: '添加角色',
                    permission: true
                },
                component: () => import (
                /* webpackChunkName: "permission" */
                "../views/System/AddAuthGroup.vue")
            }
        
        ,  {
                path: "/upload",
                name: "upload",
                meta: {
                    title: '上传插件'
                },
                component: () => import (
                /* webpackChunkName: "upload" */
                "../views/Upload.vue")
            }, {
                path: "/icon",
                name: "icon",
                meta: {
                    title: '求组'
                },
                component: () => import (
                /* webpackChunkName: "icon" */
                "../views/Icon.vue")
            }, {
                path: '/404',
                name: '404',
                meta: {
                    title: '找不到页面'
                },
                component: () => import (/* webpackChunkName: "404" */
                '../views/404.vue')
            }, {
                path: '/403',
                name: '403',
                meta: {
                    title: '没有权限'
                },
                component: () => import (/* webpackChunkName: "403" */
                '../views/403.vue')
            },
            {
                path: '/RentShow',
                name: 'RentShow',
                meta: {
                    title: '求租表的显示'
                },
                component: () => import (/* webpackChunkName: "403" */
                '../views/Rent/RentShow.vue')
            }
        ]
    }, {
        path: "/login",
        name: "Login",
        meta: {
            title: '登录'
        },
        component: () => import (
        /* webpackChunkName: "login" */
        "../views/Login.vue")
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | vue-manage-system`;
    const role = localStorage.getItem('ms_username');
    if (!role && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin'
            ? next()
            : next('/403');
    } else {
        next();
    }
});

export default router;